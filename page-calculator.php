<?php
/**
 * Template Name: Calculator Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <div id="app">
            <steps></steps>
        </div>
    </main>

<?php get_footer(); ?>
