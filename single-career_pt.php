<?php get_header(); ?>

<?php
setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
?>

<main class="pt-140">
    <div class="container container_content">
        <div class="info-line mb-5">
            <a class="back-btn mb-3 mb-md-0" href="#" onclick="history.back(-1)">zu allen Stellen</a>
            <div class="mb-3 mb-md-0">
                <span class="txt-light-grey mr-3">Hinzugefügt am</span>
                <span class="txt-grey"><?php echo get_the_date('M j, Y'); ?></span>
            </div>
            <div>
                <span class="txt-dark-grey mr-3">Teilen</span>
                <?php
                global $wp;
                $url = home_url( $wp->request );
                ?>
                <a class="mr-1" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&newsid=<?php echo get_the_title(); ?>" target="_blank"
                   onclick="window.open(this.href, 'Share','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"
                >
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.8613 0.00332907L9.70259 0C7.27738 0 5.7101 1.54552 5.7101 3.93762V5.75313H3.53963C3.35207 5.75313 3.2002 5.89927 3.2002 6.07954V8.71001C3.2002 8.89028 3.35225 9.03626 3.53963 9.03626H5.7101V15.6738C5.7101 15.854 5.86197 16 6.04953 16H8.88137C9.06892 16 9.2208 15.8539 9.2208 15.6738V9.03626H11.7586C11.9461 9.03626 12.098 8.89028 12.098 8.71001L12.0991 6.07954C12.0991 5.99299 12.0632 5.91009 11.9997 5.84884C11.9361 5.78758 11.8495 5.75313 11.7595 5.75313H9.2208V4.2141C9.2208 3.47438 9.4042 3.09886 10.4067 3.09886L11.8609 3.09836C12.0483 3.09836 12.2002 2.95222 12.2002 2.77211V0.329578C12.2002 0.149642 12.0485 0.00366197 11.8613 0.00332907Z"
                              fill="#3B5998"></path>
                    </svg>
                </a>
                <a class="mr-1" href="https://twitter.com/share?url=<?php echo $url; ?>&text=<?php echo get_the_title(); ?>&hashtags=Confitech" target="_blank"
                   onclick="window.open(this.href, 'Share','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"
                >
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7455 3.88894C15.39 4.04055 15.021 4.16015 14.6423 4.24674C15.0596 3.86542 15.3733 3.38845 15.5503 2.85608C15.6008 2.7041 15.4292 2.57267 15.2865 2.65405C14.7597 2.95456 14.1922 3.18001 13.5999 3.32419C13.5655 3.33255 13.5301 3.33679 13.4945 3.33679C13.3866 3.33679 13.2817 3.29826 13.1992 3.22833C12.5689 2.69412 11.7584 2.3999 10.9168 2.3999C10.5526 2.3999 10.185 2.45449 9.82398 2.56217C8.7055 2.89581 7.84249 3.78314 7.57172 4.8779C7.47014 5.28853 7.44264 5.69954 7.48992 6.09948C7.49534 6.14544 7.47275 6.17751 7.45883 6.19256C7.43437 6.21893 7.39969 6.23405 7.36368 6.23405C7.35966 6.23405 7.35553 6.23387 7.35143 6.23349C4.90311 6.01472 2.69532 4.8803 1.13474 3.03924C1.05516 2.94533 0.902553 2.95685 0.839123 3.06149C0.533521 3.56572 0.372021 4.14245 0.372021 4.7293C0.372021 5.62867 0.747723 6.47623 1.40032 7.09411C1.12588 7.03153 0.860266 6.93297 0.61353 6.80117C0.494161 6.73739 0.347289 6.81937 0.345612 6.95068C0.328526 8.28675 1.14188 9.47561 2.3603 10.011C2.33577 10.0115 2.31123 10.0118 2.28665 10.0118C2.09352 10.0118 1.89796 9.99384 1.70549 9.95847C1.57106 9.93378 1.45813 10.0564 1.49987 10.1818C1.89535 11.3692 2.95844 12.2445 4.23062 12.4433C3.17479 13.1249 1.94524 13.4845 0.66553 13.4845L0.266578 13.4843C0.143659 13.4843 0.0396982 13.5613 0.00837342 13.6758C-0.0224832 13.7886 0.0343539 13.9076 0.139173 13.9662C1.5812 14.7733 3.22605 15.1999 4.8966 15.1999C6.35887 15.1999 7.72671 14.9208 8.96214 14.3704C10.0947 13.8659 11.0955 13.1446 11.9368 12.2266C12.7205 11.3715 13.3334 10.3844 13.7583 9.29271C14.1633 8.25209 14.3774 7.14172 14.3774 6.08162V6.03112C14.3773 5.8609 14.4574 5.70074 14.597 5.59171C15.127 5.17789 15.5885 4.69077 15.9686 4.14387C16.069 3.99947 15.9107 3.81848 15.7455 3.88894Z"
                              fill="#76A9EA"></path>
                    </svg>
                </a>
                <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $url; ?>" target="_blank"
                   onclick="window.open(this.href, 'Share','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"
                >
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.9961 16V15.9993H16V10.1049C16 7.2213 15.4024 5 12.1569 5C10.5967 5 9.54974 5.88932 9.1223 6.73244H9.07717V5.26921H6V15.9993H9.20418V10.6862C9.20418 9.28723 9.45948 7.93449 11.1273 7.93449C12.7707 7.93449 12.7952 9.53099 12.7952 10.7759V16H15.9961Z"
                              fill="#0077B5"></path>
                        <path d="M0 5H3V16H0L0 5Z" fill="#0077B5"></path>
                        <path d="M1.55556 0C0.696815 0 0 0.693567 0 1.5483C0 2.40304 0.696815 3.11111 1.55556 3.11111C2.4143 3.11111 3.11111 2.40304 3.11111 1.5483C3.11057 0.693567 2.41376 0 1.55556 0Z"
                              fill="#0077B5"></path>
                    </svg>
                </a>
            </div>
        </div>
    </div>

    <div class="container container_sm-content">
        <div class="d-flex flex-column justify-content-center align-items-center text-center pb-110">
            <div class="title-def mb-5"><?php echo get_the_title(); ?></div>
            <?php if (get_field('description')) : ?>
                <div class="txt-def mb-5"><?php echo get_field('description'); ?></div>
            <?php endif; ?>
            <?php if (get_field('link')) : $btn = get_field('link'); ?>
                <a class="btn-def"
                    <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                   href="<?php echo $btn['url']; ?>"
                >
                    <?php echo $btn['title']; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>

    <div class="pt-140 pb-110 light-grey-bg">
        <div class="container container_sm-content">
            <?php if (get_field('top_description')) : ?>
                <div class="txt-def mb-4">
                    <?php echo get_field('top_description'); ?>
                </div>
            <?php endif; ?>
            <?php if (have_rows('statements_r')) : ?>
                <?php while (have_rows('statements_r')) : the_row(); ?>
                    <div class="mb-110">
                        <?php if (get_sub_field('title')) : ?>
                            <div class="title-def mb-3">
                                <?php echo get_sub_field('title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('labels')) : ?>
                            <ul class="check-list check-list_sm txt-dark-grey">
                                <?php while (have_rows('labels')) : the_row(); ?>
                                    <li>
                                        <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.2332 6.29226L11.8633 5.97125L12.2332 6.29226C12.6163 5.85084 12.5838 5.17549 12.1549 4.77519C11.7156 4.36512 11.034 4.40442 10.6418 4.85635L6.33409 9.81993L4.32017 7.73142C3.90387 7.29971 3.22113 7.29971 2.80483 7.73142C2.39839 8.15292 2.39839 8.82902 2.80483 9.25052L5.61733 12.1672C6.04976 12.6156 6.76334 12.595 7.17072 12.1256L12.2332 6.29226Z"
                                                  fill="#0D39BE" stroke="#0D39BE"></path>
                                        </svg>
                                        <span>
                                            <?php echo get_sub_field('statement_title'); ?>
                                        </span>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php if (get_field('bottom_description')) : ?>
                <div class="txt-def mt-5">
                    <?php echo get_field('bottom_description'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>
