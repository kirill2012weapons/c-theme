<?php
$menusList = get_field('mr_dm_rel');
?>

<header class="header <?php if (!get_field('mr_dm_th')): ?> header_color<?php endif; ?>">
    <div class="container">
        <div class="header__container">
            <a class="header__logo <?php if (!get_field('mr_dm_th')): ?> header__logo_color<?php else: ?> dark-logo-fix<?php endif; ?>" href="<?php echo home_url(); ?>"></a>

            <?php if (!empty($menusList)): ?>

                <?php
                /** @var WP_Post $menusListItem */
                $menusListItem = $menusList[0];
                $mID           = $menusListItem->ID;
                $menus         = get_field('menus_list', $mID);
                $menusRedBtn   = get_field('menus_red_button', $mID);
                ?>
                <div class="header__menu">
                    <?php $iterator = 0; ?>
                    <?php if (!empty($menus)): ?>
                        <ul>
                            <?php foreach ($menus as $menuItem): ?>
                                <li>
                                    <?php if ($menuItem['header_link']) : $btn = $menuItem['header_link']; ?>
                                        <a
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                            href="<?php if (!empty($menuItem['sub_menu_list'])): ?><?php echo 'javascript:void(0);'; ?><?php else: ?><?php echo $btn['url']; ?><?php endif; ?>"
                                            <?php if (!empty($menuItem['sub_menu_list'])): ?> class="arrow" data-trig-menu="<?php echo $iterator; ?>"<?php endif; ?>
                                        ><?php echo $btn['title']; ?></a>
                                    <?php endif; ?>
                                </li>
                                <?php $iterator++ ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if ($menusRedBtn) : $btn = $menusRedBtn; ?>
                        <a
                            class="btn-def btn-def_calc"
                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                            href="<?php echo $btn['url']; ?>"
                        ><?php echo $btn['title']; ?></a>
                    <?php endif; ?>
                </div>
                <?php if (!empty($menus)): ?>
                    <div class="header__submenu d-none">
                        <?php $iterator = 0; ?>
                        <?php foreach ($menus as $menuItem): ?>
                            <?php if (!empty($menuItem['sub_menu_list'])): ?>
                                <ul class="d-none d-flex" data-menu="<?php echo $iterator; ?>">
                                    <?php foreach ($menuItem['sub_menu_list'] as $menuSubItem): ?>
                                        <li>
                                            <?php if ($menuSubItem['link']) : $btn = $menuSubItem['link']; ?>
                                                <a
                                                    class="dynamic-link"
                                                    <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                                    href="<?php echo $btn['url']; ?>"
                                                >
                                                    <?php if ($menuSubItem['icon']): ?>
                                                        <img src="<?php echo $menuSubItem['icon']['sizes']['thumbnail_40_40'] ?>" alt="">
                                                    <?php endif; ?>
                                                    <span><?php echo $btn['title']; ?></span>
                                                </a>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <?php $iterator++ ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="header__btns">
                <a id="search-click" class="mr-4" href="#">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.3069 7.65347C13.3069 9.20627 12.6818 10.6117 11.6679 11.6341C11.2888 12.0165 10.8556 12.3449 10.3811 12.6068C9.57293 13.0529 8.64387 13.3069 7.65347 13.3069C4.53115 13.3069 2 10.7758 2 7.65347C2 4.53115 4.53115 2 7.65347 2C10.7758 2 13.3069 4.53115 13.3069 7.65347Z" stroke="white" stroke-width="2"></path>
                        <path d="M12.1035 12.1035L14.5914 14.5914" stroke="white" stroke-width="2" stroke-linecap="round"></path>
                    </svg>
                </a>
                <div id="nav-open-burger" class="nav-btn">
                    <div class="nav-icon"><span></span><span></span><span></span></div>
                </div>
            </div>
        </div>
        <?php echo get_template_part('template-parts/boorger', 'main') ?>
    </div>
</header>
