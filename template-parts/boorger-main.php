<?php
$menusList = get_field('mr_dbm_rel');
?>

<?php if (!empty($menusList)): ?>
    <?php
    /** @var WP_Post $menusListItem */
    $menusListItem = $menusList[0];
    $mID           = $menusListItem->ID;
    ?>

    <div class="menu-drop">
        <?php
        $mainLinkRepeater          = get_field('bmo_mc_r', $mID);
        $bootstrapColumnNumber     = 12 / (count($mainLinkRepeater) + 1);
        $greyColumn                = get_field('bmo_gc_g', $mID);
        ?>
        <div class="container h-100 pc-menu">
            <div class="row h-100">
                <?php $foreachIndex = 0; ?>
                <?php $isShowRedBtn = true; ?>
                <?php foreach ($mainLinkRepeater as $mlr): ?>
                    <div class="col-<?php echo $bootstrapColumnNumber; ?> menu-border <?php if ($foreachIndex !== 0): ?> centered<?php endif; ?>">
                        <div class="menu-col h-100">
                            <?php if (!empty($mlr['column_title'])): ?>
                                <div class="menu-col__title">
                                    <?php echo $mlr['column_title']; ?>
                                </div>
                            <?php endif; ?>
                            <?php foreach ($mlr['sub_list_r'] as $slr): ?>
                                <?php if (!empty($slr['sub_title'])): ?>
                                    <div class="sm-subtitle mb-3">
                                        <?php echo $slr['sub_title']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($slr['link_list_r'])): ?>
                                    <ul class="menu-col__list">
                                        <?php foreach ($slr['link_list_r'] as $llr): ?>
                                            <li>
                                                <?php if ($llr['link']) : $btn = $llr['link']; ?>
                                                    <a
                                                        class="dynamic-link"
                                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                                       href="<?php echo $btn['url']; ?>"
                                                    >
                                                        <?php if (!empty($llr['icon'])): ?>
                                                            <img src="<?php echo $llr['icon']['sizes']['thumbnail_25_25']; ?>" alt="img"/>
                                                        <?php endif; ?>
                                                        <span><?php echo $btn['title']; ?></span>
                                                    </a>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if (get_field('bmo_red_button', $mID) && $isShowRedBtn)
                                : $btn = get_field('bmo_red_button', $mID); $isShowRedBtn = false; ?>
                                <a class="btn-def mw-180 mt-3"
                                    <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                   href="<?php echo $btn['url']; ?>"
                                >
                                    <?php echo $btn['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $foreachIndex++ ?>
                <?php endforeach; ?>

                <div class="col-<?php echo $bootstrapColumnNumber; ?> centered grey-bg pl-50">
                    <?php echo App\WPMLSwitcher\WPMLSwitcher::getSwitcher() ?>
                    <div class="menu-col w-100 h-100">
                        <?php if ($greyColumn['show_search_input']): ?>
                            <?php get_search_form(); ?>
                        <?php endif; ?>
                        <?php if (!empty($greyColumn['main_links_r'])): ?>
                            <ul class="menu-links">
                                <?php foreach ($greyColumn['main_links_r'] as $gmlr): ?>
                                    <li>
                                        <?php if ($gmlr['link']) : $btn = $gmlr['link']; ?>
                                            <a
                                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                                href="<?php echo $btn['url']; ?>"
                                            >
                                                <?php echo $btn['title']; ?>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <?php if (!empty($greyColumn['addition_information_r'])): ?>
                            <ul class="contact-list">
                                <?php foreach ($greyColumn['addition_information_r'] as $gair): ?>
                                    <li>
                                        <?php if (!empty($gair['icon'])): ?>
                                            <img src="<?php echo $gair['icon']['sizes']['thumbnail_16_16']; ?>" alt="">
                                        <?php endif; ?>
                                        <?php if (!empty($gair['text'])): ?>
                                            <?php if (strpos($gair['text'], '@') == 0): ?>
                                                <span class="ml-2">
                                                    <?php echo $gair['text']; ?>
                                                </span>
                                            <?php else: ?>
                                                <a class="ml-2" href="mailto:<?php echo $gair['text']; ?>">
                                                    <?php echo $gair['text']; ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <?php if (!empty($greyColumn['addition_information_r'])): ?>
                            <div class="social-block mt-auto">
                                <?php foreach ($greyColumn['social_r'] as $gsr): ?>
                                    <?php if ($gsr['link']) : $btn = $gsr['link']; ?>
                                        <a
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                            href="<?php echo $btn['url']; ?>"
                                        >
                                            <?php if (!empty($gsr['icon'])): ?>
                                                <img src="<?php echo $gsr['icon']['sizes']['thumbnail_16_16']; ?>" alt="img"/>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($greyColumn['bread_crumbles_r'])): ?>
                            <ul class="privacy-links mt-4">
                                <?php foreach ($greyColumn['bread_crumbles_r'] as $gbcr): ?>
                                    <li>
                                        <?php if ($gbcr['link']) : $btn = $gbcr['link']; ?>
                                            <a
                                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                                href="<?php echo $btn['url']; ?>"
                                            >
                                                <?php echo $btn['title']; ?>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container h-100 d-block d-md-none">
            <div class="mobile-menu">
                <?php if ($greyColumn['show_search_input']): ?>
                    <?php get_search_form(); ?>
                <?php endif; ?>
                <?php $isShowRedBtn = true; ?>
                <?php foreach ($mainLinkRepeater as $mlr): ?>
                    <div class="menu-item">
                        <?php if (!empty($mlr['column_title'])): ?>
                            <a class="menu-item__title arrow" href="javascript:void(0);">
                                <?php echo $mlr['column_title']; ?>
                            </a>
                        <?php endif; ?>
                        <?php $countSubListR = 1; ?>
                        <?php foreach ($mlr['sub_list_r'] as $slr): ?>
                            <div class="menu-item__cont">
                                <?php if (!empty($slr['sub_title'])): ?>
                                    <div class="sm-subtitle mb-3">
                                        <?php echo $slr['sub_title']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($slr['link_list_r'])): ?>
                                    <ul class="menu-col__list">
                                        <?php foreach ($slr['link_list_r'] as $llr): ?>
                                            <li>
                                                <?php if ($llr['link']) : $btn = $llr['link']; ?>
                                                    <a
                                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                                        href="<?php echo $btn['url']; ?>"
                                                    >
                                                        <?php if (!empty($llr['icon'])): ?>
                                                            <img src="<?php echo $llr['icon']['sizes']['thumbnail_25_25']; ?>" alt="img"/>
                                                        <?php endif; ?>
                                                        <span><?php echo $btn['title']; ?></span>
                                                    </a>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <?php if ($countSubListR == count($mlr['sub_list_r'])) : ?>
                                    <?php if (get_field('bmo_red_button', $mID) && $isShowRedBtn)
                                        : $btn = get_field('bmo_red_button', $mID); $isShowRedBtn = false; ?>
                                        <a class="btn-def mw-180 mt-3"
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                           href="<?php echo $btn['url']; ?>"
                                        >
                                            <?php echo $btn['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <?php $countSubListR++; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>

                <?php if (!empty($greyColumn['main_links_r'])): ?>
                    <?php foreach ($greyColumn['main_links_r'] as $gmlr): ?>
                        <div class="menu-item">
                            <?php if ($gmlr['link']) : $btn = $gmlr['link']; ?>
                                <a class="menu-item__title"
                                    <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                    href="<?php echo $btn['url']; ?>"
                                >
                                    <?php echo $btn['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>


                <?php if (!empty($greyColumn['addition_information_r'])): ?>
                    <ul class="contact-list mt-4">
                        <?php foreach ($greyColumn['addition_information_r'] as $gair): ?>
                            <li>
                                <?php if (!empty($gair['icon'])): ?>
                                    <img src="<?php echo $gair['icon']['sizes']['thumbnail_16_16']; ?>" alt="">
                                <?php endif; ?>
                                <?php if (!empty($gair['text'])): ?>
                                    <?php if (strpos($gair['text'], '@') == 0): ?>
                                        <span class="ml-2">
                                                    <?php echo $gair['text']; ?>
                                                </span>
                                    <?php else: ?>
                                        <a class="ml-2" href="mailto:<?php echo $gair['text']; ?>">
                                            <?php echo $gair['text']; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <?php if (!empty($greyColumn['bread_crumbles_r'])): ?>
                    <ul class="privacy-links mt-4">
                        <?php foreach ($greyColumn['bread_crumbles_r'] as $gbcr): ?>
                            <li>
                                <?php if ($gbcr['link']) : $btn = $gbcr['link']; ?>
                                    <a
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                        href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <div class="mt-5 copyright-txt">© Copyright - confitech Dienstleistungen </div>
            </div>
        </div>
    </div>
<?php endif; ?>