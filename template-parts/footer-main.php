<?php
$menusList = get_field('mr_dfm_rel');
?>

<?php if (!empty($menusList)): ?>
    <?php
    /** @var WP_Post $menusListItem */
    $menusListItem = $menusList[0];
    $mID           = $menusListItem->ID;
    ?>
    <div class="row">
        <?php
        $checkTechnology = true;

        $fRowsCounts      = count(get_field('fmo_g_c_g_rr', $mID)) + 2;
        $dBootstrapColumn = 12 / $fRowsCounts;
        ?>
        <?php if (have_rows('fmo_g_c_g_rr', $mID)): ?>
            <?php while (have_rows('fmo_g_c_g_rr', $mID)): the_row(); ?>
                <div class="col-<?php echo floor($dBootstrapColumn + 4); ?> col-md-<?php echo floor($dBootstrapColumn + 2); ?> col-xl-<?php echo ceil($dBootstrapColumn); ?>">
                    <div class="footer-col">
                        <?php if (get_sub_field('title')): ?>
                            <div class="footer-col__title">
                                <?php echo get_sub_field('title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('list_links_r')): ?>
                            <ul class="footer-col__list">
                                <?php while (have_rows('list_links_r')): the_row(); ?>
                                    <li>
                                        <?php if (get_sub_field('link')) : $btn = get_sub_field('link'); ?>
                                            <a  class="dynamic-link"
                                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                               href="<?php echo $btn['url']; ?>"
                                            >
                                                <?php echo $btn['title']; ?>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                        <?php if ($checkTechnology): ?>
                                <?php if (get_field('fmo_b_g_main_link', $mID)) : $btn = get_field('fmo_b_g_main_link', $mID); ?>
                                    <a class="footer-col__title mt-auto"
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                       href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                        <?php $checkTechnology = false; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        <div class="col-<?php echo floor($dBootstrapColumn + 4); ?> col-md-<?php echo floor($dBootstrapColumn + 2); ?> col-xl-<?php echo ceil($dBootstrapColumn); ?>">
            <div class="footer-col">
                <?php if (have_rows('fmo_g_c_5_g_list_links', $mID)): ?>
                    <ul class="section-list">
                        <?php while (have_rows('fmo_g_c_5_g_list_links', $mID)): the_row(); ?>
                            <li>
                                <?php if (get_sub_field('link')) : $btn = get_sub_field('link'); ?>
                                    <a
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                        href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-<?php echo floor($dBootstrapColumn + 8); ?> col-md-<?php echo floor($dBootstrapColumn + 2); ?> col-xl-<?php echo ceil($dBootstrapColumn); ?>">
            <div class="footer-col">
                <?php if (get_field('fmo_c_g_title', $mID)): ?>
                    <div class="footer-col__title ml-1">
                        <?php echo get_field('fmo_c_g_title', $mID); ?>
                    </div>
                <?php endif; ?>
                <ul class="contact-list">
                    <?php if (get_field('fmo_c_g_address_g_text', $mID)): ?>
                        <li>
                            <?php if (get_field('fmo_c_g_address_g_icon', $mID)): ?>
                                <img src="<?php echo get_field('fmo_c_g_address_g_icon', $mID)['url'] ?>" alt="">
                            <?php endif; ?>
                            <span>
                                <?php echo get_field('fmo_c_g_address_g_text', $mID) ?>
                            </span>
                        </li>
                    <?php endif; ?>
                    <?php if (get_field('fmo_c_g_phone_g_text', $mID)): ?>
                        <li>
                            <?php if (get_field('fmo_c_g_phone_g_icon', $mID)): ?>
                                <img src="<?php echo get_field('fmo_c_g_phone_g_icon', $mID)['url'] ?>" alt="">
                            <?php endif; ?>
                            <span>
                                <?php echo get_field('fmo_c_g_phone_g_text', $mID) ?>
                            </span>
                        </li>
                    <?php endif; ?>
                    <?php if (get_field('fmo_c_g_fax_g_text', $mID)): ?>
                        <li>
                            <?php if (get_field('fmo_c_g_fax_g_icon', $mID)): ?>
                                <img src="<?php echo get_field('fmo_c_g_fax_g_icon', $mID)['url'] ?>" alt="">
                            <?php endif; ?>
                            <span>
                                <?php echo get_field('fmo_c_g_fax_g_text', $mID) ?>
                            </span>
                        </li>
                    <?php endif; ?>
                    <?php if (get_field('fmo_c_g_fax_g_text', $mID)): ?>
                        <li>
                            <a href="mailto:<?php echo get_field('fmo_c_g_email_g_text', $mID) ?>">
                                <?php if (get_field('fmo_c_g_email_g_icon', $mID)): ?>
                                    <img src="<?php echo get_field('fmo_c_g_email_g_icon', $mID)['url'] ?>" alt="">
                                <?php endif; ?>
                                <span>
                                    <?php echo get_field('fmo_c_g_email_g_text', $mID) ?>
                                </span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between align-items-center mt-3 mt-md-5 flex-column-reverse flex-md-row">
        <div class="d-flex">
            <?php
            $breadCrumblesCount = count(
                get_field('fmo_b_g_bread_crumbles_r', $mID)
            );
            ?>
            <?php while (have_rows('fmo_b_g_bread_crumbles_r', $mID)): the_row(); ?>
                <?php if (get_sub_field('link')) : $btn = get_sub_field('link'); ?>
                    <?php if ($btn['url'] == '#'): ?>
                        <span class="txt-grey fs-13">
                            <?php echo $btn['title']; ?>
                        </span>
                    <?php else: ?>
                        <a class="link-grey fs-13"
                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                           href="<?php echo $btn['url']; ?>"
                        >
                            <?php echo $btn['title']; ?>
                        </a>
                    <?php endif; ?>
                    <?php if ($breadCrumblesCount !== get_row_index()): ?>
                        <span class="txt-grey fs-13 ml-4 mr-4">/</span>
                    <?php endif; ?>
                <?php endif; ?>

            <?php endwhile; ?>
        </div>
        <div class="footer-socials mb-4 mb-md-0">
            <?php while (have_rows('fmo_b_g_icons_links_r', $mID)): the_row(); ?>
                <?php if (get_sub_field('link')) : $btn = get_sub_field('link'); ?>
                    <a class="mr-3"
                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                       href="<?php echo $btn['url']; ?>"
                    >
                        <?php if (get_sub_field('icon')): ?>
                            <img class="mw-100" src="<?php echo get_sub_field('icon')['url']; ?>" alt="img"/>
                        <?php endif; ?>
                    </a>

                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
