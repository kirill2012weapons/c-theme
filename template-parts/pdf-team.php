<?php
/** @var $model \App\Model\ContactCalculator\ContactCalculatorModel */
$model   = $args['model'];
$orderID = $args['order_id'];
?>

<html><head>
    <meta charset="utf-8">
    <title>PDF Confitech</title>
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            font-size: 14px;
            line-height: 1.1;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #000;
        }
        .logo {
            text-align: right;
            margin-bottom: 15px;
        }
        .mb-10 {
            margin-bottom: 5px;
        }
        .mb-20{
            margin-bottom: 20px;
        }
        .mb-50{
            margin-bottom: 50px;
        }
        .header{
            margin-bottom: 30px;
        }
        .text-right{
            text-align: right;
        }
        .title{
            font-size: 24px;
            text-align: center;
            font-weight: bold;
        }
        .table-footer {
            padding-left: 0;
            margin-left: 0;
            width: 100%;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            border-spacing: 0;
            margin-bottom: 5px;
        }
        .table-footer td{
            padding: 0 7px;
            margin: 0;
            font-size: 13px;
        }
        .table-def {
            display: table;
            width: 100%;
            border: none;
            font-size: 14px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            border-collapse: separate;
            border-spacing: 0 2px;
        }

        .table-def__row {
            display: table-row;
            background: #f2f2f3;
        }

        .table-def__cell {
            display: table-cell;
            vertical-align: middle;
            text-align: left;
        }

        .table-def__cell_cont {
            padding: 8px 15px;
        }

        .font-weight-bold{
            font-weight: bold;
        }
        .txt-red{
            color: #AA002D;
        }
        .fs-20 {
            font-size: 20px;
        }
        p {
            margin-top: 0;
            margin-bottom: 5px;
        }
    </style>
</head>
<body>

<div class="invoice-box">
    <div class="header">
        <div class="logo"><img src="/wp-content/themes/confitech/assets/images/color.png" style="width:100%; max-width:200px;"></div>
        <p class="mb-10">Kay Thomas</p>
        <p class="mb-10">Vertrieb Web-Entwicklung</p>
        <p class="mb-10">Tel.: 	+49 (731) 94645-22</p>
        <p class="mb-10">Handy: +49 (176) 64180920</p>
        <p class="mb-10">Fax: 	+49 (731) 94645-20</p>
        <p class="mb-10">E-Mail: kay.thomas@confitech.de</p>
        <p class="mb-10">Confitech GmbH</p>
        <p class="mb-10">Lehrer Strasse 1</p>
        <p class="mb-10">89081 Ulm</p>
    </div>
    <div class="main">
        <div class="order-number mb-20">Angebotsnummer:  <?php echo $orderID; ?></div>
        <div class="text-right mb-50">Ulm, <?php echo date('d.m.Y') ?></div>
        <div class="title mb-50">Angebot personelle Unterstützung</div>
        <p class="mb-10">Lieber Kunde, liebe Kundin,</p>
        <p class="mb-20">nachfolgend ist Ihr vorläufiges Angebot aufgeführt, auf Grundlage Ihrer Angaben:</p>
        <table class="table-def mb-50">
            <tbody>
                <tr class="table-def__row">
                    <th class="table-def__cell table-def__cell_cont">Spezialisten</th>
                    <th class="table-def__cell table-def__cell_cont">Erfahrung</th>
                    <th class="table-def__cell table-def__cell_cont">Dauer (Wochen)</th>
                    <th class="table-def__cell table-def__cell_cont">Voraussichtliche Kosten</th>
                </tr>
                <?php
                $total = 0;
                ?>
                <?php foreach ($model->getRows() as $information): ?>
                    <?php /** @var $information App\Model\ContactCalculator\RowInformation */ ?>
                    <tr class="table-def__row">
                        <td class="table-def__cell table-def__cell_cont"><?php echo $information->getPosition(); ?></td>
                        <td class="table-def__cell table-def__cell_cont"><?php echo $information->getExperience(); ?></td>
                        <td class="table-def__cell table-def__cell_cont"><?php echo $information->getDays(); ?></td>
                        <td class="table-def__cell table-def__cell_cont font-weight-bold"><?php if ($information->getPrice()): ?>€ <?php echo $information->getPrice(); ?><?php endif; ?></td>
                    </tr>
                    <?php
                    $total += $information->getPrice();
                    ?>
                <?php endforeach; ?>
            <tr class="table-def__row">
                <td class="table-def__cell table-def__cell_cont"></td>
                <td class="table-def__cell table-def__cell_cont font-weight-bold"></td>
                <td class="table-def__cell table-def__cell_cont font-weight-bold"></td>
                <td class="table-def__cell table-def__cell_cont font-weight-bold fs-20 txt-red">€ <?php echo $total; ?></td>
            </tr>
            </tbody>
        </table>
        <p>Die tatsächlichen Projektkosten können abweichen und hängen von Ihren individuellen Anforderungen ab.</p>
        <p>Alle genannten Preise verstehen sich zzgl. der gesetzl. MwSt.</p>
        <p class="mb-20">Preisänderungen, technische Änderungen und Irrtümer vorbehalten.</p>
        <p class="mb-50">Für eine genaue Kostenberechnung, kontaktieren Sie bitte unseren Vertrieb!</p>
        <p>Mit freundlichen Grüßen,</p>
        <p class="mb-50">Kay Thomas</p>
    </div>
</div>
</body></html>
