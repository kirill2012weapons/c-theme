<?php
/**
 * Template Name: Engineering Solution Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <?php if (get_field('estobg_g_show')) : ?>
            <div class="main-banner main-banner_es-sol"
                <?php if (get_field('estobg_g_background_image')) : ?>
                    style="background-image: url(<?php echo get_field('estobg_g_background_image')['url']; ?>)"
                <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center h-100">
                        <?php if (get_field('estobg_g_banner_title')) : ?>
                            <div class="title-def mb-5">
                                <?php echo get_field('estobg_g_banner_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('estobg_g_banner_description')) : ?>
                            <p class="text-center mw-40 mb-5">
                                <?php echo get_field('estobg_g_banner_description'); ?>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('estobg_g_button')) : $btn = get_field('estobg_g_button'); ?>
                            <a class="btn-def"
                               <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                               href="<?php echo $btn['url']; ?>"
                            >
                                <?php echo $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('estoc_r')) : ?>
            <div class="pt-140">
                <div class="container container_content">
                    <?php while (have_rows('estoc_r')) : the_row(); ?>
                        <?php
                        $image          = get_sub_field('image')                 ? get_sub_field('image')                 : false;
                        $title          = get_sub_field('title')                 ? get_sub_field('title')                 : false;
                        $description    = get_sub_field('description')           ? get_sub_field('description')           : false;
                        $subDescription = get_sub_field('sub_image_description') ? get_sub_field('sub_image_description') : false;
                        ?>
                        <?php if (get_row_index() % 2 == 0) : ?>
                            <div class="row align-items-center flex-wrap-reverse flex-md-wrap pb-110">
                                <div class="col-md-6 position-relative">
                                    <?php if ($subDescription) : ?>
                                        <div class="img-text img-text_top">
                                            <?php echo $subDescription; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($image) : ?>
                                        <img class="mw-100 solution-image"
                                             src="<?php echo $image['sizes']['thumbnail_500_330']; ?>"
                                             alt="<?php echo $image['alt'] ?>"
                                        >
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-6 col-lg-5 offset-lg-1 mb-4 mb-md-0">
                                    <?php if ($title) : ?>
                                        <div class="title-def mb-4">
                                            <?php echo $title; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($description) : ?>
                                        <div class="txt-def">
                                            <?php echo $description; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="row align-items-center pb-110">
                                <div class="col-md-5">
                                    <?php if ($title) : ?>
                                        <div class="title-def mb-4">
                                            <?php echo $title; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($description) : ?>
                                        <div class="txt-def">
                                            <?php echo $description; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-6 offset-md-1 position-relative">
                                    <?php if ($image) : ?>
                                        <img class="mw-100 solution-image"
                                             src="<?php echo $image['sizes']['thumbnail_500_330']; ?>"
                                             alt="<?php echo $image['alt'] ?>"
                                        >
                                    <?php endif; ?>
                                    <?php if ($subDescription) : ?>
                                        <div class="img-text img-text_bottom">
                                            <?php echo $subDescription; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (get_field('estowwub_g_show')) : ?>
            <div class="pt-110">
                <div class="container container_content">
                    <div class="contact-banner mb-200"
                        <?php if (get_field('estowwub_g_background_image')) : ?>
                            style="background-image: url(<?php echo get_field('estowwub_g_background_image')['url']; ?>)"
                        <?php endif; ?>
                    >
                        <div class="d-flex flex-column justify-content-center align-items-center h-100">
                            <?php if (get_field('estowwub_g_title')) : ?>
                                <div class="title-def mb-4">
                                    <?php echo get_field('estowwub_g_title'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if (get_field('estowwub_g_description')) : ?>
                                <p class="mb-4">
                                    <?php echo get_field('estowwub_g_description'); ?>
                                </p>
                            <?php endif; ?>
                            <?php if (get_field('estowwub_g_button')) : $btn = get_field('estobg_g_button'); ?>
                                <a class="btn-def"
                                   <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                   href="<?php echo $btn['url']; ?>"
                                >
                                    <?php echo $btn['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('estooc_r')) : ?>
            <div class="pt-140 pb-110">
                <div class="container container_content">
                    <div>
                        <div class="title-def text-center mb-5">
                            <?php _e('Unsere Kunden'); ?>
                        </div>
                        <div class="logos-block">
                            <?php while (have_rows('estooc_r')) : the_row(); ?>
                                <div class="mr-2">
                                    <?php if (get_sub_field('logo')) : ?>
                                        <img src="<?php echo get_sub_field('logo')['url'] ?>" alt="<?php echo get_sub_field('logo')['alt'] ?>">
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>
