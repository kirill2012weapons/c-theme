<?php
$searchTargetForm = isset($_GET['target']) ? $_GET['target'] : '';
?>
<form class="search-field" role="search" method="get" id="searchform" action="<?php echo home_url( '/search/' ) ?>" >
    <input type="text"
           class="input input-def"
           placeholder="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>"
           value="<?php echo $searchTargetForm; ?>"
           name="target"
           <?php if (isset($args['red'])): ?> style="border: 1px solid #AA002D;" <?php endif; ?>
           id="target" />
    <a href="#"
       onclick="$('#searchsubmit').click(); return false;">
        <img src="/wp-content/themes/confitech_html/build/img/header/search.svg" alt="img"/>
    </a>
    <input type="hidden" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
</form>