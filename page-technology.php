<?php
/**
 * Template Name: Technology Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main class="pt-140 pb-110">
        <div class="container container_content">
            <?php if (get_field('tto_g_title')): ?>
                <div class="title-def text-center mb-6">
                    <?php echo get_field('tto_g_title'); ?>
                </div>
            <?php endif; ?>
            <?php
            $gallery = get_field('tto_g_list');
            ?>
            <?php if ($gallery): ?>
                <div class="technology">
                    <?php foreach( $gallery as $image ): ?>
                        <div class="technology__item">
                            <img src="<?php echo esc_url($image['sizes']['thumbnail_33_33']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </main>

<?php get_footer(); ?>