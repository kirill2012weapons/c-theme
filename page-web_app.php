<?php
/**
 * Template Name: Wep App Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <?php if (get_field('wato_g_ms_g_active')): ?>
            <div class="main-banner main-banner_web"
                 <?php if (get_field('wato_g_ms_g_background_image')): ?>
                    style="background-image: url(<?php echo get_field('wato_g_ms_g_background_image')['url'] ?>);"
                 <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center h-100">
                        <?php if (get_field('wato_g_ms_g_title')): ?>
                            <div class="title-def mb-5 w-75 text-center">
                                <?php echo get_field('wato_g_ms_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('wato_g_ms_g_description')): ?>
                            <p class="mw-40 mb-5 text-center">
                                <?php echo get_field('wato_g_ms_g_description'); ?>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('wato_g_ms_g_link')) : $btn = get_field('wato_g_ms_g_link'); ?>
                            <a class="btn-def"
                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                               href="<?php echo $btn['url']; ?>"
                            >
                                <?php echo $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="pt-140 pb-110">
            <div class="container container_content">
                <?php if (get_field('wato_g_cs_g_active')): ?>
                    <div>
                        <div class="calc-banner calc-banner_left">
                            <?php if (get_field('wato_g_cs_g_image')): ?>
                                <img class="calc-banner__img" src="<?php echo get_field('wato_g_cs_g_image')['url'] ?>" alt="<?php echo get_field('wato_g_cs_g_image')['title'] ?>"/>
                            <?php endif; ?>
                            <div class="calc-banner__block">
                                <?php if (get_field('wato_g_cs_g_title')): ?>
                                    <div class="title-def mb-5 mt-5 mt-md-0">
                                        <?php echo get_field('wato_g_cs_g_title') ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('wato_g_cs_g_description')): ?>
                                    <div class="txt-def fs-20 mb-5">
                                        <?php echo get_field('wato_g_cs_g_description') ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('wato_g_cs_g_link')) : $btn = get_field('wato_g_cs_g_link'); ?>
                                    <a class="btn-def"
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                       href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('wato_g_ads_g_active')): ?>
                    <div class="mt-150">
                        <?php if (get_field('wato_g_ads_g_title')): ?>
                            <div class="title-def text-center m-0-auto mb-5 w-75">
                                <?php echo get_field('wato_g_ads_g_title') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (have_rows('wato_g_ads_g_content_repeater')): ?>
                            <div class="timeline">
                                <?php while (have_rows('wato_g_ads_g_content_repeater')): the_row(); ?>
                                    <div class="row justify-content-end justify-content-md-around align-items-start timeline-nodes">
                                        <div class="col-10 col-lg-5 order-3 order-md-1 timeline-content">
                                            <div class="d-flex align-items-start">
                                                <?php if (get_sub_field('logo')): ?>
                                                    <img class="mw-100 mr-3" src="<?php echo get_sub_field('logo')['url']; ?>" alt="<?php echo get_sub_field('logo')['title']; ?>"/>
                                                <?php endif; ?>
                                                <?php if (get_sub_field('title')): ?>
                                                    <div class="md-subtitle mb-3">
                                                        <?php echo get_sub_field('title'); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <?php if (get_sub_field('description')): ?>
                                                <div class="txt-def">
                                                    <?php echo get_sub_field('description'); ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-2 col-sm-1 px-md-3 order-2 timeline-image"></div>
                                        <div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date"></div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('wato_g_rs_g_active')): ?>
                    <div class="mt-150">
                        <div class="row align-items-center">
                            <?php if (get_field('wato_g_rs_g_title')): ?>
                                <div class="col-md-6">
                                    <div class="title-def pr-md-5 mb-4 mb-md-0">
                                        <?php echo get_field('wato_g_rs_g_title'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (get_field('wato_g_rs_g_description')): ?>
                                <div class="col-md-6">
                                    <div class="txt-def pl-md-5">
                                        <?php echo get_field('wato_g_rs_g_description'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('wato_g_rss_g_active')): ?>
                    <div class="mt-150">
                        <div class="modernization">
                            <div class="row align-items-center modernization__scheme">
                                <div class="col-12 text-white h3">
                                    <div class="modernization__arrow_1">
                                        <?php if (get_field('wato_g_rss_g_title')): ?>
                                            <div class="relative d-none d-lg-block">
                                                <?php echo get_field('wato_g_rss_g_title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_field('wato_g_rss_g_description')): ?>
                                            <div class="relative d-none d-lg-block">
                                                <?php echo get_field('wato_g_rss_g_description'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <div class="modernization__arrow modernization__arrow_2 d-flex flex-column">
                                                <?php if (get_field('wato_g_rss_g_t_b_g_title_right')): ?>
                                                    <div class="title-def mb-4">
                                                        <?php echo get_field('wato_g_rss_g_t_b_g_title_right'); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('wato_g_rss_g_t_b_g_description_right')): ?>
                                                    <div class="txt-def mb-3">
                                                        <?php echo get_field('wato_g_rss_g_t_b_g_description_right'); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="modernization__arrow modernization__arrow_3 d-flex flex-column">
                                                <?php if (get_field('wato_g_rss_g_t_b_g_title_left')): ?>
                                                    <div class="title-def mb-4">
                                                        <?php echo get_field('wato_g_rss_g_t_b_g_title_left'); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('wato_g_rss_g_t_b_g_description_left')): ?>
                                                    <div class="txt-def mb-3">
                                                        <?php echo get_field('wato_g_rss_g_t_b_g_description_left'); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <div class="modernization__arrow modernization__arrow_4 d-flex flex-column">
                                                <?php if (get_field('wato_g_rss_g_t_t_g_title_right')): ?>
                                                    <div class="title-def mb-4">
                                                        <?php echo get_field('wato_g_rss_g_t_t_g_title_right'); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('wato_g_rss_g_t_t_g_description_right')): ?>
                                                    <div class="txt-def mb-3">
                                                        <?php echo get_field('wato_g_rss_g_t_t_g_description_right'); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="modernization__arrow modernization__arrow_5 d-flex flex-column">
                                                <?php if (get_field('wato_g_rss_g_t_t_g_title_left')): ?>
                                                    <div class="title-def mb-4">
                                                        <?php echo get_field('wato_g_rss_g_t_t_g_title_left'); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('wato_g_rss_g_t_t_g_description_left')): ?>
                                                    <div class="txt-def mb-3">
                                                        <?php echo get_field('wato_g_rss_g_t_t_g_description_left'); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="modernization__arrow_6"></div>
                                </div>
                            </div>
                        </div>
                        <div class="reasons">
                            <img class="dots-bg" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/dots.svg'); ?>" alt="img"/>
                            <div class="reasons__title">
                                <?php if (get_field('wato_g_rss_g_title')): ?>
                                    <div class="md-subtitle mb-2">
                                        <?php echo get_field('wato_g_rss_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('wato_g_rss_g_description')): ?>
                                    <div class="txt-def">
                                        <?php echo get_field('wato_g_rss_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="triangle triangle_1">
                                <img class="mw-100" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/triangle.png'); ?>" alt="img"/>
                                <div class="triangle__text">
                                    <?php if (get_field('wato_g_rss_g_t_t_g_description_left')): ?>
                                        <p>
                                            <?php echo get_field('wato_g_rss_g_t_t_g_description_left'); ?>
                                        </p>
                                    <?php endif; ?>
                                    <?php if (get_field('wato_g_rss_g_t_t_g_title_left')): ?>
                                        <p class="red">
                                            <?php echo get_field('wato_g_rss_g_t_t_g_title_left'); ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="triangle triangle_2">
                                <img class="mw-100" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/triangle.png'); ?>" alt="img"/>
                                <div class="triangle__text">
                                    <?php if (get_field('wato_g_rss_g_t_t_g_description_right')): ?>
                                        <p>
                                            <?php echo get_field('wato_g_rss_g_t_t_g_description_right'); ?>
                                        </p>
                                    <?php endif; ?>
                                    <?php if (get_field('wato_g_rss_g_t_t_g_title_right')): ?>
                                        <p class="red">
                                            <?php echo get_field('wato_g_rss_g_t_t_g_title_right'); ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="black-triangle">
                                <!--img(src='../img/webapp/new/Fish.svg' alt='img')-->
                                <img class="mw-100 black-triangle__sm" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/black-sm.svg'); ?>" alt="img"/>
                                <img class="mw-100 black-triangle__lg" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/black.svg'); ?>" alt="img"/>
                                <?php if (get_field('wato_g_rss_g_arrow_text')): ?>
                                    <div class="black-triangle__text">
                                        <?php echo get_field('wato_g_rss_g_arrow_text'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="triangle triangle_3"><img class="mw-100" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/triangle-bot.svg'); ?>" alt="img"/>
                                <div class="triangle__text">
                                    <?php if (get_field('wato_g_rss_g_t_b_g_title_left')): ?>
                                        <p class="red">
                                            <?php echo get_field('wato_g_rss_g_t_b_g_title_left'); ?>
                                        </p>
                                    <?php endif; ?>
                                    <?php if (get_field('wato_g_rss_g_t_b_g_description_left')): ?>
                                        <p>
                                            <?php echo get_field('wato_g_rss_g_t_b_g_description_left'); ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="triangle triangle_4"><img class="mw-100" src="<?php echo home_url('wp-content/themes/confitech_html/build/img/webapp/triangle-bot.svg'); ?>" alt="img"/>
                                <div class="triangle__text">
                                    <?php if (get_field('wato_g_rss_g_t_b_g_title_right')): ?>
                                        <p class="red">
                                            <?php echo get_field('wato_g_rss_g_t_b_g_title_right'); ?>
                                        </p>
                                    <?php endif; ?>
                                    <?php if (get_field('wato_g_rss_g_t_b_g_description_right')): ?>
                                        <p>
                                            <?php echo get_field('wato_g_rss_g_t_b_g_description_right'); ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php while (have_rows('wato_g_ss_g')): the_row(); ?>
                    <div class="mt-150">
                        <?php if (get_sub_field('title')): ?>
                            <div class="title-def mb-6">
                                <?php echo get_sub_field('title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('statements_r')): ?>
                            <div class="row">
                                <?php while (have_rows('statements_r')): the_row(); ?>
                                    <div class="col-md-6 col-lg-4 d-flex d-md-block flex-column text-center text-md-left">
                                        <?php if (get_sub_field('image')): ?>
                                            <img class="mw-100" src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>"/>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('title')): ?>
                                            <div class="sm-subtitle mt-3 mb-3">
                                                <?php echo get_sub_field('title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('description')): ?>
                                            <div class="txt-def mb-5">
                                                <?php echo get_sub_field('description'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>

                <?php if (get_field('wato_g_rm_g_active')): ?>
                    <div class="mt-150">
                        <div class="row align-items-center flex-wrap-reverse flex-md-wrap">
                            <?php if (get_field('wato_g_rm_g_image')): ?>
                                <div class="col-md-6">
                                    <img class="mw-100" src="<?php echo get_field('wato_g_rm_g_image')['url']; ?>" alt="<?php echo get_field('wato_g_rm_g_image')['title']; ?>"/>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="pl-md-4">
                                    <?php if (get_field('wato_g_rm_g_title')): ?>
                                        <div class="title-def mb-5">
                                            <?php echo get_field('wato_g_rm_g_title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php while (have_rows('wato_g_rm_g_information_r')): the_row(); ?>
                                        <div class="mb-5">
                                            <?php if (get_sub_field('title')): ?>
                                                <div class="md-subtitle mb-3">
                                                    <?php echo get_sub_field('title'); ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (get_sub_field('description')): ?>
                                                <div class="txt-def">
                                                    <?php echo get_sub_field('description'); ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('wato_g_mss_g_active')): ?>
                    <div class="mt-150">
                        <?php if (get_field('wato_g_mss_g_title')): ?>
                            <div class="title-def mb-6">
                                <?php echo get_field('wato_g_mss_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('wato_g_mss_g_information_r')): ?>
                            <div class="row">
                                <?php while (have_rows('wato_g_mss_g_information_r')): the_row(); ?>
                                    <div class="col-lg-4 mb-4">
                                        <?php if (get_sub_field('title')): ?>
                                            <div class="md-subtitle mb-3">
                                                <?php echo get_sub_field('title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('description')): ?>
                                            <div class="txt-def">
                                                <?php echo get_sub_field('description'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>
