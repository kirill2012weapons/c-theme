<?php
/**
 * Template Name: Impressum Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <div class="pt-140 pb-110">
            <div class="container container_content">
                <div class="row flex-wrap-reverse flex-md-wrap">
                    <?php if (have_rows('ito_g_lc_r')): ?>
                        <div class="col-md-8">
                            <?php while (have_rows('ito_g_lc_r')): the_row(); ?>
                                <div class="mb-5">
                                    <?php if (get_sub_field('main_title')): ?>
                                        <div class="title-def mb-5">
                                            <?php echo get_sub_field('main_title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('secondary_title')): ?>
                                        <div class="md-subtitle mb-5">
                                            <?php echo get_sub_field('secondary_title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('content')): ?>
                                        <div class="txt-def">
                                            <?php echo get_sub_field('content'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (have_rows('ito_g_rc_r')): ?>
                        <div class="col-md-4">
                            <?php while (have_rows('ito_g_rc_r')): the_row(); ?>
                                <div class="mb-6">
                                    <div class="mb-5">
                                        <?php if (get_sub_field('main_title')): ?>
                                            <div class="title-def mb-5">
                                                <?php echo get_sub_field('main_title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('secondary_title')): ?>
                                            <div class="md-subtitle mb-5">
                                                <?php echo get_sub_field('secondary_title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('content')): ?>
                                            <div>
                                                <?php echo get_sub_field('content'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
