<?php
/**
 * Template Name: Home Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

<div id="fullpage">

    <?php if (have_rows('screens')) : ?>
        <?php while(have_rows('screens')) : the_row(); ?>
            <?php $anchors[sanitize_title(get_sub_field('title_label'))] = get_sub_field('title_label'); ?>
            <?php include(__DIR__ . '/layouts/home_template/' . get_row_layout() . '.php'); ?>
        <?php endwhile; ?>
    <?php endif; ?>

</div>

    <?php if (!empty($anchors)) : ?>
        <div class="home-nav">
            <ul id="home-nav-list">
                <?php foreach ($anchors as $anchorKey => $anchorValue) : ?>
                    <li data-menuanchor="<?php echo $anchorKey; ?>">
                        <a href="<?php echo '#' . $anchorKey; ?>">
                            <?php echo $anchorValue; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>