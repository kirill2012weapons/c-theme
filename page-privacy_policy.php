<?php
/**
 * Template Name: Privacy Policy Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <div class="pt-140 pb-110">
            <div class="container container_content">
                <div class="row flex-wrap-reverse flex-md-wrap">
                    <div class="col-md-8">
                        <?php if (have_rows('ppto_tm_r')) : ?>
                            <?php while( have_rows('ppto_tm_r') ) : the_row(); ?>

                                <?php if (get_sub_field('main_title')) : ?>
                                    <div class="title-def mb-5"><?php echo get_sub_field('main_title'); ?></div>
                                <?php endif ?>

                                <?php if (have_rows('sg_r')) : ?>
                                    <?php $bluesTitles = []; ?>
                                    <?php while( have_rows('sg_r') ) : the_row(); ?>

                                        <?php if (get_sub_field('blue_title')) : ?>
                                            <div class="subtitle-def txt-blue mb-5" id="<?php echo sanitize_title(get_sub_field('blue_title')); ?>">
                                                <?php echo get_sub_field('blue_title'); ?>
                                            </div>
                                            <?php $bluesTitles[] = get_sub_field('blue_title'); ?>
                                        <?php endif ?>

                                        <?php if (get_sub_field('content')) : ?>
                                            <div class="post-content mb-5"><?php echo get_sub_field('content'); ?></div>
                                        <?php endif ?>

                                    <?php endwhile; ?>
                                <?php endif ?>

                            <?php endwhile; ?>
                        <?php endif ?>
                    </div>

                    <?php if (!empty($bluesTitles)) : ?>
                        <div class="col-md-4 sticky-cont mb-5 mb-md-0">
                            <ul class="privacy-list">
                                <?php foreach ($bluesTitles as $title) : ?>
                                    <li>
                                        <a class="privacy-list__link" href="<?php echo '#' . $title ?>" data-value="<?php echo sanitize_title($title); ?>">
                                            <?php echo $title; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
