<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&amp;display=swap" rel="stylesheet"/>
    <link rel="shortcut icon" href="<?php echo home_url('/wp-content/themes/confitech/assets/images/favicon.ico'); ?>" type="image/x-icon">
    <?php wp_head(); ?>
</head>
<body>
    <div id="loader"></div>
    <?php if (!is_404()): ?>
        <?php echo get_template_part('template-parts/dinamic', 'menu') ?>
    <?php endif; ?>