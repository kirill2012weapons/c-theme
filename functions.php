<?php
/**
 *          | Define all constants for confitech_html
 *          |       theme folder
 */
define( '__HTML_CSS__',   get_theme_root_uri() . '/confitech_html/build/css'   );
define( '__HTML_FONTS__', get_theme_root_uri() . '/confitech_html/build/fonts' );
define( '__HTML_IMG__',   get_theme_root_uri() . '/confitech_html/build/img'   );
define( '__HTML_JS__',    get_theme_root_uri() . '/confitech_html/build/js'    );
/**
 *          | Define all constants for confitech
 *          |       theme folder
 */
define( '__PHP_JS__',     get_theme_root_uri() . '/confitech/assets/js'     );
define( '__PHP_IMG__',    get_theme_root_uri() . '/confitech/assets/images' );
define( '__PHP_CSS__',    get_theme_root_uri() . '/confitech/assets/css'    );

/**
 *          | Init CSS Enqueue
 */

use App\Repository\Calculator\CalculatorAppRepository;
use App\Repository\Calculator\CalculatorTeamAppRepository;
use App\Services\JMS\Serializer;
use App\WpEnqueue\{EnqueueCss, EnqueueJs, EnqueueDTO};

EnqueueCss::init()
    ->add(new EnqueueDTO('bootstrap_min_css', __PHP_CSS__  . '/bootstrap.min.css'))
    ->add(new EnqueueDTO('fullpage_min_css',  __PHP_CSS__  . '/fullpage.min.css'))
    ->add(new EnqueueDTO('aos_css',           'https://unpkg.com/aos@2.3.1/dist/aos.css'))
    ->add(new EnqueueDTO('app_css',           __HTML_CSS__ . '/app.css'))
    ->add(new EnqueueDTO('custom_style_css',  __PHP_CSS__  . '/style.css'))
/**
 *          | Init CSS Enqueue For Admin
 */
    ->addAdmin(new EnqueueDTO('custom_style_css',  __PHP_CSS__  . '/style-admin.css'))
;

/**
 *          | Init JS Enqueue
 */
EnqueueJs::init()
    ->add(new EnqueueDTO('jquery_3_5_1_js',  __PHP_JS__  . '/jquery-3.5.1.js'))
    ->add(new EnqueueDTO('easings_min_js',   __PHP_JS__  . '/easings.min.js'))
    ->add(new EnqueueDTO('fullpage_min_js',  __PHP_JS__  . '/fullpage.min.js'))
    ->add(new EnqueueDTO('fullpage_js',      __HTML_JS__ . '/fullpage.js'))
    ->add(new EnqueueDTO('script_js',        __HTML_JS__ . '/script.js'))
    ->add(new EnqueueDTO('js_cookie_min_js', 'https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js'))
    ->add(new EnqueueDTO('aos_js',           'https://unpkg.com/aos@2.3.1/dist/aos.js'))
    ->add(new EnqueueDTO('custom_script_js', __PHP_JS__  . '/script.js'))

    ->addCalculatorScript(new EnqueueDTO('js_npm_vue_js',                 'https://cdn.jsdelivr.net/npm/vue'))
    ->addCalculatorScript(new EnqueueDTO('axios_min_js',                  'https://unpkg.com/axios/dist/axios.min.js'))
    ->addCalculatorScript(new EnqueueDTO('steps_js',                      __HTML_JS__  . '/steps.js'))
    ->addCalculatorScript(new EnqueueDTO('steps_form_js',                 __HTML_JS__  . '/form.js'))
    ->addCalculatorScript(new EnqueueDTO('loader_js',                     __HTML_JS__  . '/loader.js'))
 ;

/**
 *          | Add Thumbnail sizes
 */
add_image_size( 'thumbnail_500_330', 500, 330 );
add_image_size( 'thumbnail_415_610', 415, 610 );
add_image_size( 'thumbnail_430_440', 430, 440 );
add_image_size( 'thumbnail_500_525', 500, 525 );
add_image_size( 'thumbnail_80_80',   80, 80   );
add_image_size( 'thumbnail_31_31',   31, 31   );
add_image_size( 'thumbnail_33_33',   33, 33   );
add_image_size( 'thumbnail_570_490', 570, 490 );
add_image_size( 'thumbnail_620_360', 620, 360 );
add_image_size( 'thumbnail_111_111', 111, 111 );
add_image_size( 'thumbnail_460_420', 460, 420 );
add_image_size( 'thumbnail_325_220', 325, 220 );
add_image_size( 'thumbnail_25_25',   25, 25 );
add_image_size( 'thumbnail_16_16',   16, 16 );
add_image_size( 'thumbnail_40_40',   40, 40 );

/**
 *          | Disable Posts
 */
(new App\DisabledFeature\WPDisablePosts());

/**
 *          | Register Career PT
 *          | Register Mails PT
 *          | Register Menu PT
 */
App\PostType\CareerPostType::init();
App\PostType\MailsPostType::init();
App\PostType\MenuPostType::init();
App\PostType\MailsCalculatorPostType::init();
App\PostType\MenuBurgerPostType::init();
App\PostType\MenuFooterPostType::init();

/**
 *          | Register Taxonomies
 *          |   - Office
 *          |   - Position
 */
App\PostTaxonomy\OfficeTaxonomy::init();
App\PostTaxonomy\PositionTaxonomy::init();

add_action('init', 'registerSession');
function registerSession() {
    if( ! session_id() ) {
        session_start();
    }
}

/**
 *          | Register Option menu
 *          |          Option calculator
 */
App\Option\CalculatorOptions::init()
    ->register();
App\Option\CalculatorTeamOptions::init()
    ->register();
App\Option\ContactOptions::init()
    ->register();

/**
 *          | AJAX Handlers
 *          |       ->route :: add router handler for routes
 */
use App\Services\REST\Handlers as RouteHandlers;
try {
    App\Services\REST\AjaxHandler::init()

        ->route('get_app_calculator_information', RouteHandlers\GetAppCalculatorInformationHandler::class)
        ->route('get_app_team_calculator_information', RouteHandlers\GetAppTeamCalculatorInformationHandler::class)

        ->route('get_pdf_team_view', RouteHandlers\GetPdfTeamHandler::class)
        ->route('get_pdf_app_view', RouteHandlers\GetPdfAppHandler::class)

        ->route('get_contact_pdf_team_view', RouteHandlers\ContactPdfTeamHandler::class)
        ->route('get_contact_pdf_app_view', RouteHandlers\ContactPdfAppHandler::class)

        ->register();
} catch (Exception $e) {
    exit();
}
