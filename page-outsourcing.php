<?php
/**
 * Template Name: Outsourcing Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <?php if (get_field('oto_g_ms_g_active')): ?>
            <div class="main-banner main-banner_outsource"
                <?php if (get_field('oto_g_ms_g_background_image')): ?>
                    style="background-image: url(<?php echo get_field('oto_g_ms_g_background_image')['url'] ?>);"
                <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center h-100">
                        <?php if (get_field('oto_g_ms_g_title')): ?>
                            <div class="title-def text-center mb-5">
                                <?php echo get_field('oto_g_ms_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('oto_g_ms_g_description')): ?>
                            <p class="mw-40 mb-5 text-center">
                                <?php echo get_field('oto_g_ms_g_description'); ?>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('oto_g_ms_g_link')) : $btn = get_field('oto_g_ms_g_link'); ?>
                            <a class="btn-def text-center"
                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                               href="<?php echo $btn['url']; ?>"
                            >
                                <?php echo $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="pt-140 pb-110">
            <div class="container container_content">

                <?php if (get_field('oto_g_ots_g_active')): ?>
                    <div>
                        <div class="row align-items-center">
                            <?php if (get_field('oto_g_ots_g_oto_g_ots_gl_g_title')): ?>
                                <div class="col-xl-6">
                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gl_g_title')): ?>
                                        <div class="title-def title-def_w75 mb-5">
                                            <?php echo get_field('oto_g_ots_g_oto_g_ots_gl_g_title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gl_g_link')) : $btn = get_field('oto_g_ots_g_oto_g_ots_gl_g_link'); ?>
                                        <a class="btn-red"
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                           href="<?php echo $btn['url']; ?>"
                                        >
                                            <span><?php echo $btn['title']; ?></span>
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.66675 8.2666L6.66675 4.66667" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M9.66675 8.26671L6.66675 11.8667" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                            </svg>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <div class="col-xl-6">
                                <div class="info-circle ml-auto mr-auto ml-xl-auto mr-xl-0 d-none d-md-flex">
                                    <div class="d-flex flex-column align-items-center w-50">
                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_logo')): ?>
                                            <img src="<?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_logo')['url']; ?>" alt="">
                                        <?php endif; ?>
                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_title')): ?>
                                            <span class="text-center mt-3">
                                                <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_title'); ?>
                                            </span>
                                        <?php endif; ?>
                                    </div>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_html')): ?>
                                        <div class="info-circle-item info-circle-item_appdev">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                    <path d="M17.208 18.1749L23.208 12.1749L17.208 6.17493" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M7.20801 6.17493L1.20801 12.1749L7.20801 18.1749" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <line x1="12.7332" y1="9.48799" x2="10.5211" y2="14.6497" stroke="#AA002D" stroke-width="2" stroke-linecap="round"></line>
                                                </g>
                                                <defs>
                                                    <clippath id="clip0">
                                                        <rect width="24" height="24" fill="white" transform="translate(0.208008 0.174927)"></rect>
                                                    </clippath>
                                                </defs>
                                            </svg>
                                            <div class="info-circle-item__box">
                                                <span>
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_html'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_aim')): ?>
                                        <div class="info-circle-item info-circle-item_outsource">
                                            <svg class="target" width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="12.208" cy="12.0107" r="9" stroke="#0D39BE" stroke-width="2"></circle>
                                                <circle cx="12.208" cy="12.0107" r="6" stroke="#AA002D" stroke-width="2"></circle>
                                                <circle cx="12.208" cy="12.0107" r="2" stroke="#0D39BE" stroke-width="2"></circle>
                                            </svg>
                                            <div class="info-circle-item__box">
                                                <span>
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_aim'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_people')): ?>
                                        <div class="info-circle-item info-circle-item_staff">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M14.0129 16.2056V15.23C14.0129 14.7125 13.8073 14.2162 13.4414 13.8503C13.0755 13.4844 12.5792 13.2788 12.0617 13.2788H8.15923C7.64173 13.2788 7.14543 13.4844 6.77951 13.8503C6.41358 14.2162 6.20801 14.7125 6.20801 15.23V16.2056" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M10.1104 11.3277C11.188 11.3277 12.0616 10.4541 12.0616 9.37651C12.0616 8.29888 11.188 7.42529 10.1104 7.42529C9.03277 7.42529 8.15918 8.29888 8.15918 9.37651C8.15918 10.4541 9.03277 11.3277 10.1104 11.3277Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M17.94 16.2057V15.2301C17.9397 14.7978 17.7958 14.3778 17.5309 14.0361C17.266 13.6944 16.8952 13.4504 16.4766 13.3423" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M14.5254 7.48877C14.9451 7.59623 15.3171 7.84033 15.5828 8.18258C15.8484 8.52483 15.9926 8.94576 15.9926 9.37901C15.9926 9.81227 15.8484 10.2332 15.5828 10.5754C15.3171 10.9177 14.9451 11.1618 14.5254 11.2693" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M4.20801 7.13269V4.01074H7.7202" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path d="M20.5981 16.8888V20.0107H17.0859" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                            </svg>
                                            <div class="info-circle-item__box">
                                                <span>
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_people'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_repair')): ?>
                                        <div class="info-circle-item info-circle-item_settings">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M14.9079 6.47417C14.7246 6.6611 14.622 6.91242 14.622 7.17417C14.622 7.43593 14.7246 7.68725 14.9079 7.87417L16.5079 9.47417C16.6948 9.6574 16.9461 9.76003 17.2079 9.76003C17.4696 9.76003 17.7209 9.6574 17.9079 9.47417L21.6779 5.70417C22.1807 6.81536 22.3329 8.05341 22.1143 9.25332C21.8957 10.4532 21.3166 11.558 20.4541 12.4205C19.5917 13.2829 18.4869 13.862 17.287 14.0806C16.0871 14.2993 14.8491 14.147 13.7379 13.6442L6.82786 20.5542C6.43004 20.952 5.89047 21.1755 5.32786 21.1755C4.76525 21.1755 4.22569 20.952 3.82786 20.5542C3.43004 20.1563 3.20654 19.6168 3.20654 19.0542C3.20654 18.4916 3.43004 17.952 3.82786 17.5542L10.7379 10.6442C10.235 9.53299 10.0828 8.29494 10.3014 7.09503C10.52 5.89512 11.0992 4.79033 11.9616 3.9279C12.824 3.06546 13.9288 2.48634 15.1287 2.26772C16.3286 2.04909 17.5667 2.20134 18.6779 2.70417L14.9179 6.46417L14.9079 6.47417Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                <circle cx="4.20801" cy="8.17419" r="3" fill="#AA002D"></circle>
                                            </svg>
                                            <div class="info-circle-item__box">
                                                <span>
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_repair'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_box')): ?>
                                        <div class="info-circle-item info-circle-item_codepen">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                    <path d="M12.208 15.757L22.208 9.25696V16.257L12.208 22.757V15.757Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M12.208 15.757L22.208 9.25696V16.257L12.208 22.757V15.757Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M2.20801 16.257L12.208 22.757V15.757L2.20801 9.25696V16.257Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M2.20801 9.25696L12.208 2.75696V9.75696L2.20801 16.257V9.25696Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M12.208 9.75696L22.208 16.257V9.25696L12.208 2.75696V9.75696Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path d="M2.20801 9.25696L12.208 15.7569L22.208 9.25692" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                </g>
                                                <defs>
                                                    <clippath id="clip0">
                                                        <rect width="24" height="24" fill="white" transform="translate(0.208008 0.756958)"></rect>
                                                    </clippath>
                                                </defs>
                                            </svg>
                                            <div class="info-circle-item__box">
                                                <span>
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_box'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_title')): ?>
                                        <div class="info-circle-round info-circle-round_top">
                                            <div class="sm-subtitle mb-2">
                                                <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_title'); ?>
                                            </div>
                                            <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_description')): ?>
                                                <div class="txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_description'); ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_inner_description')): ?>
                                                <div class="info-circle-item__box">
                                                    <span>
                                                        <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_inner_description'); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_title')): ?>
                                        <div class="info-circle-round info-circle-round_bottom">
                                            <div class="sm-subtitle mb-2">
                                                <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_title'); ?>
                                            </div>
                                            <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_description')): ?>
                                                <div class="txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_description'); ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_inner_description')): ?>
                                                <div class="info-circle-item__box">
                                                    <span>
                                                        <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_inner_description'); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="d-md-none">
                                    <div class="info-mobile">
                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_title')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img info-item__img_circle">
                                                    <h3>
                                                        <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_title'); ?>
                                                    </h3>
                                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_description')): ?>
                                                        <div class="txt-def">
                                                            <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_description'); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>


                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_top_big_circle_inner_description'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_html')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img appdev">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <g clip-path="url(#clip0)">
                                                            <path d="M17.208 18.1749L23.208 12.1749L17.208 6.17493" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M7.20801 6.17493L1.20801 12.1749L7.20801 18.1749" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <line x1="12.7332" y1="9.48799" x2="10.5211" y2="14.6497" stroke="#AA002D" stroke-width="2" stroke-linecap="round"></line>
                                                        </g>
                                                        <defs>
                                                            <clippath id="clip0">
                                                                <rect width="24" height="24" fill="white" transform="translate(0.208008 0.174927)"></rect>
                                                            </clippath>
                                                        </defs>
                                                    </svg>
                                                </div>
                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_html'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_aim')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img outsource">
                                                    <svg class="target" width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <circle cx="12.208" cy="12.0107" r="9" stroke="#0D39BE" stroke-width="2"></circle>
                                                        <circle cx="12.208" cy="12.0107" r="6" stroke="#AA002D" stroke-width="2"></circle>
                                                        <circle cx="12.208" cy="12.0107" r="2" stroke="#0D39BE" stroke-width="2"></circle>
                                                    </svg>
                                                </div>
                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_aim'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_people')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img staff">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                                d="M14.0129 16.2056V15.23C14.0129 14.7125 13.8073 14.2162 13.4414 13.8503C13.0755 13.4844 12.5792 13.2788 12.0617 13.2788H8.15923C7.64173 13.2788 7.14543 13.4844 6.77951 13.8503C6.41358 14.2162 6.20801 14.7125 6.20801 15.23V16.2056"
                                                                stroke="#0D39BE"
                                                                stroke-width="2"
                                                                stroke-linecap="round"
                                                                stroke-linejoin="round"
                                                        ></path>
                                                        <path
                                                                d="M10.1104 11.3277C11.188 11.3277 12.0616 10.4541 12.0616 9.37651C12.0616 8.29888 11.188 7.42529 10.1104 7.42529C9.03277 7.42529 8.15918 8.29888 8.15918 9.37651C8.15918 10.4541 9.03277 11.3277 10.1104 11.3277Z"
                                                                stroke="#0D39BE"
                                                                stroke-width="2"
                                                                stroke-linecap="round"
                                                                stroke-linejoin="round"
                                                        ></path>
                                                        <path d="M17.94 16.2057V15.2301C17.9397 14.7978 17.7958 14.3778 17.5309 14.0361C17.266 13.6944 16.8952 13.4504 16.4766 13.3423" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path
                                                                d="M14.5254 7.48877C14.9451 7.59623 15.3171 7.84033 15.5828 8.18258C15.8484 8.52483 15.9926 8.94576 15.9926 9.37901C15.9926 9.81227 15.8484 10.2332 15.5828 10.5754C15.3171 10.9177 14.9451 11.1618 14.5254 11.2693"
                                                                stroke="#0D39BE"
                                                                stroke-width="2"
                                                                stroke-linecap="round"
                                                                stroke-linejoin="round"
                                                        ></path>
                                                        <path d="M4.20801 7.13269V4.01074H7.7202" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path d="M20.5981 16.8888V20.0107H17.0859" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    </svg>
                                                </div>
                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_people'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_repair')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img setting">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M14.9079 6.47417C14.7246 6.6611 14.622 6.91242 14.622 7.17417C14.622 7.43593 14.7246 7.68725 14.9079 7.87417L16.5079 9.47417C16.6948 9.6574 16.9461 9.76003 17.2079 9.76003C17.4696 9.76003 17.7209 9.6574 17.9079 9.47417L21.6779 5.70417C22.1807 6.81536 22.3329 8.05341 22.1143 9.25332C21.8957 10.4532 21.3166 11.558 20.4541 12.4205C19.5917 13.2829 18.4869 13.862 17.287 14.0806C16.0871 14.2993 14.8491 14.147 13.7379 13.6442L6.82786 20.5542C6.43004 20.952 5.89047 21.1755 5.32786 21.1755C4.76525 21.1755 4.22569 20.952 3.82786 20.5542C3.43004 20.1563 3.20654 19.6168 3.20654 19.0542C3.20654 18.4916 3.43004 17.952 3.82786 17.5542L10.7379 10.6442C10.235 9.53299 10.0828 8.29494 10.3014 7.09503C10.52 5.89512 11.0992 4.79033 11.9616 3.9279C12.824 3.06546 13.9288 2.48634 15.1287 2.26772C16.3286 2.04909 17.5667 2.20134 18.6779 2.70417L14.9179 6.46417L14.9079 6.47417Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <circle cx="4.20801" cy="8.17419" r="3" fill="#AA002D"></circle>
                                                    </svg>
                                                </div>
                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_repair'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_logo')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img info-item__img_circle">
                                                    <img src="<?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_logo')['url']; ?>" alt="">
                                                </div>

                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_center_title'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_box')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img codepen">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <g clip-path="url(#clip0)">
                                                            <path d="M12.208 15.757L22.208 9.25696V16.257L12.208 22.757V15.757Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M12.208 15.757L22.208 9.25696V16.257L12.208 22.757V15.757Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M2.20801 16.257L12.208 22.757V15.757L2.20801 9.25696V16.257Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M2.20801 9.25696L12.208 2.75696V9.75696L2.20801 16.257V9.25696Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M12.208 9.75696L22.208 16.257V9.25696L12.208 2.75696V9.75696Z" stroke="#0D39BE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M2.20801 9.25696L12.208 15.7569L22.208 9.25692" stroke="#AA002D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </g>
                                                        <defs>
                                                            <clippath id="clip0">
                                                                <rect width="24" height="24" fill="white" transform="translate(0.208008 0.756958)"></rect>
                                                            </clippath>
                                                        </defs>
                                                    </svg>
                                                </div>
                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_circle_box'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_title')): ?>
                                            <div class="info-item">
                                                <div class="info-item__img info-item__img_circle">
                                                    <h3>
                                                        <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_title'); ?>
                                                    </h3>
                                                    <?php if (get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_description')): ?>
                                                        <div class="txt-def">
                                                            <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_description'); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="info-item__desc txt-def">
                                                    <?php echo get_field('oto_g_ots_g_oto_g_ots_gr_g_bottom_big_circle_inner_description'); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('oto_g_wos_g_active')): ?>
                    <div class="mt-150">
                        <div class="outsource-banner">
                                <img class="outsource-banner__img"
                                     src="<?php echo home_url('/wp-content/themes/confitech_html/build/img/outsourcing/bg.png'); ?>"
                                     alt="img"
                                />
                            <div class="outsource-banner__cont">
                                <?php if (get_field('oto_g_wos_g_title')): ?>
                                    <div class="title-def">
                                        <?php echo get_field('oto_g_wos_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (have_rows('oto_g_wos_g_statements_r')): ?>
                                    <ul class="outsource-banner__list">
                                        <?php while (have_rows('oto_g_wos_g_statements_r')): the_row(); ?>
                                            <li>
                                                <?php echo get_sub_field('title'); ?>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('oto_g_ovs_g_active')): ?>
                    <div class="mt-150">
                        <?php if (get_field('oto_g_ovs_g_title')): ?>
                            <div class="title-def mb-6">
                                <?php echo get_field('oto_g_ovs_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('oto_g_ovs_g_content_r')): ?>
                            <div class="row">
                                <?php while (have_rows('oto_g_ovs_g_content_r')): the_row(); ?>
                                    <div class="col-md-6 mb-4 mb-md-5">
                                        <div class="d-flex align-items-center">
                                            <?php if (get_sub_field('logo')): ?>
                                                <img src="<?php echo get_sub_field('logo')['sizes']['thumbnail_33_33'] ?>"
                                                     alt="<?php echo get_sub_field('logo')['title'] ?>"
                                                >
                                            <?php endif; ?>
                                            <?php if (get_sub_field('title')): ?>
                                                <span class="ml-3">
                                                    <?php echo get_sub_field('title') ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('oto_g_cs_g_active')): ?>
                    <div class="mt-150 pt-5 pt-md-0">
                        <div class="calc-banner calc-banner_left">
                            <?php if (get_field('oto_g_cs_g_image')): ?>
                                <img class="calc-banner__img"
                                     src="<?php echo get_field('oto_g_cs_g_image')['sizes']['thumbnail_570_490'] ?>"
                                     alt="<?php echo get_field('oto_g_cs_g_image')['title'] ?>"
                                >
                            <?php endif; ?>
                            <div class="calc-banner__block">
                                <?php if (get_field('oto_g_cs_g_title')): ?>
                                    <div class="title-def mb-3 mb-md-5">
                                        <?php echo get_field('oto_g_cs_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('oto_g_cs_g_description')): ?>
                                    <div class="txt-def fs-20 mb-3 mb-md-5">
                                        <?php echo get_field('oto_g_cs_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('oto_g_cs_g_link')) : $btn = get_field('oto_g_cs_g_link'); ?>
                                    <a class="btn-def"
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                       href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>
