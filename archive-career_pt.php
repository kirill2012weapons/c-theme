<?php get_header(); ?>

<main class="pt-140 pb-110">
    <div class="container container_content"><a class="back-btn mb-4" href="#" onclick="history.back(-1)">Zurück</a>
        <div class="title-def">
            Aktuelle Stellen
        </div>
        <div class="mt-5">
            <div class="d-flex flex-wrap">
                <?php if (have_posts()) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php $taxes = wp_get_post_terms(get_the_ID(), 'office_tax'); ?>
                        <a class="vacancy-item vacancy-item_half" href="<?php echo get_post_permalink() ?>">
                            <p class="font-weight-bold">
                                <?php echo get_the_title(); ?>
                            </p>
                            <?php foreach ($taxes as $tax) : ?>
                                <?php /** @var $tax WP_Term */ ?>
                                <div class="d-flex align-items-center txt-dark-grey">
                                    <svg class="mr-2" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8 2C10.7536 2 13 4.37767 13 7.32845C13 9.04361 11.5134 11.2084 8.31488 13.7526L8 14C4.58408 11.3485 3 9.09894 3 7.32845C3 4.37767 5.24638 2 8 2ZM8 5C6.89543 5 6 5.89543 6 7C6 8.10457 6.89543 9 8 9C9.10457 9 10 8.10457 10 7C10 5.89543 9.10457 5 8 5Z" fill="#5E5F63"></path>
                                    </svg>
                                    <span><?php echo $tax->name ?></span>
                                </div>
                            <?php endforeach; ?>
                        </a>
                    <?php endwhile; ?>
                <?php else: ?>
                    <p class="font-weight-bold">
                        No vacancies
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
