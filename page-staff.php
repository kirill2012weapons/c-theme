<?php
/**
 * Template Name: Staff Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>

        <?php if (get_field('sto_g_ms_g_active')): ?>
            <div class="main-banner main-banner_staff"
                <?php if (get_field('sto_g_ms_g_background_image')) : ?>
                    style="background-image: url(<?php echo get_field('sto_g_ms_g_background_image')['url']; ?>)"
                <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center h-100">
                        <?php if (get_field('sto_g_ms_g_title')) : ?>
                            <div class="title-def text-center mb-5">
                                <?php echo get_field('sto_g_ms_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('sto_g_ms_g_description')) : ?>
                            <p class="mw-40 mb-5 text-center">
                                <?php echo get_field('sto_g_ms_g_description'); ?>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('sto_g_ms_g_link')) : $btn = get_field('sto_g_ms_g_link'); ?>
                            <a class="btn-def"
                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                               href="<?php echo $btn['url']; ?>"
                            >
                                <?php echo $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="pt-140 pb-110">
            <div class="container container_content">

                <?php if (get_field('sto_g_tes_g_active')): ?>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-5">
                            <?php if (get_field('sto_g_tes_g_title')): ?>
                                <div class="title-def mb-4">
                                    <?php echo get_field('sto_g_tes_g_title'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if (get_field('sto_g_tes_g_content')): ?>
                                <p class="txt-def">
                                    <?php echo get_field('sto_g_tes_g_content'); ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <?php if (get_field('sto_g_tes_g_image')): ?>
                            <div class="col-md-6 offset-lg-1 mt-4 mt-md-0">
                                <img class="mw-100" src="<?php echo get_field('sto_g_tes_g_image')['sizes']['thumbnail_500_330']; ?>" alt="<?php echo get_field('sto_g_tes_g_image')['title']; ?>"/>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('sto_g_wss_g_active')): ?>
                    <div class="mt-150">
                        <div class="img-banner">
                            <?php if (get_field('sto_g_wss_g_background_image')): ?>
                                <img class="img-banner__pic" src="<?php echo get_field('sto_g_wss_g_background_image')['url']; ?>" alt="<?php echo get_field('sto_g_wss_g_background_image')['title']; ?>"/>
                            <?php endif; ?>
                            <div class="img-banner__block">
                                <?php if (get_field('sto_g_wss_g_title')): ?>
                                    <div class="title-def mb-5">
                                        <?php echo get_field('sto_g_wss_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('sto_g_wss_g_description')): ?>
                                    <div class="txt-def fs-20 mb-5">
                                        <?php echo get_field('sto_g_wss_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('sto_g_wss_g_link')) : $btn = get_field('sto_g_wss_g_link'); ?>
                                    <a class="btn-def"
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                       href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('sto_g_as_g_active')): ?>
                    <div class="mt-150">
                        <?php if (get_field('sto_g_as_g_title')): ?>
                            <div class="title-def text-center mb-6">
                                <?php echo get_field('sto_g_as_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('sto_g_as_g_logos_r')): ?>
                            <div class="row">
                                <?php while (have_rows('sto_g_as_g_logos_r')): the_row(); ?>
                                    <div class="col-md-6 col-xl-3 mb-5 mb-xl-0">
                                        <div class="d-flex flex-column align-items-center text-center">
                                            <img src="<?php echo get_sub_field('logo')['sizes']['thumbnail_80_80']; ?>" alt="<?php echo get_sub_field('logo')['title']; ?>">
                                            <?php if (get_sub_field('title')): ?>
                                                <span class="mt-3">
                                                    <?php echo get_sub_field('title'); ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('sto_g_tss_g_active')): ?>
                    <div class="mt-150">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-lg-5">
                                <?php if (get_field('sto_g_tss_g_title')): ?>
                                    <div class="title-def mb-4">
                                        <?php echo get_field('sto_g_tss_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('sto_g_tss_g_content')): ?>
                                    <p class="txt-def mb-3">
                                        <?php echo get_field('sto_g_tss_g_content'); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <?php if (get_field('sto_g_tss_g_image')): ?>
                                <div class="col-md-6 offset-lg-1">
                                    <img class="mw-100" src="<?php echo get_field('sto_g_tss_g_image')['sizes']['thumbnail_500_330']; ?>" alt="<?php echo get_field('sto_g_tss_g_image')['title']; ?>"/>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php while (have_rows('sto_g_is_r')): the_row(); ?>
                    <div class="mt-150">
                        <?php if (get_row_index() % 2): ?>
                            <div class="grey-wrapper grey-wrapper_padding">
                        <?php endif; ?>
                                <?php if (get_sub_field('title')): ?>
                                    <div class="title-def text-center mb-6">
                                        <?php echo get_sub_field('title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php $counts = count(get_sub_field('logos_r')); ?>
                                <?php if (have_rows('logos_r')): ?>
                                    <div class="row">
                                        <?php while (have_rows('logos_r')): the_row(); ?>
                                            <div class="col-md-6 col-xl-<?php echo 12/$counts ?> mb-5 mb-xl-0">
                                                <div class="d-flex flex-column align-items-center text-center">
                                                    <?php if (get_sub_field('title')): ?>
                                                        <img src="<?php echo get_sub_field('logo')['sizes']['thumbnail_80_80']; ?>" alt="<?php echo get_sub_field('logo')['title']; ?>"/>
                                                    <?php endif; ?>
                                                    <?php if (get_sub_field('title')): ?>
                                                        <span class="mt-3">
                                                            <?php echo get_sub_field('title'); ?>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            <?php if (get_row_index() % 2): ?>
                            </div>
                            <?php endif; ?>
                    </div>
                <?php endwhile; ?>

                <?php if (get_field('sto_g_cs_g_active')): ?>
                    <div class="mt-150">
                        <div class="calc-banner">
                            <?php if (get_field('sto_g_cs_g_image')): ?>
                                <img class="calc-banner__img" src="<?php echo get_field('sto_g_cs_g_image')['sizes']['thumbnail_620_360']; ?>" alt="<?php echo get_field('sto_g_cs_g_image')['title']; ?>"/>
                            <?php endif; ?>
                            <div class="calc-banner__block">
                                <?php if (get_field('sto_g_cs_g_title')): ?>
                                    <div class="title-def mb-5 mt-5 mt-md-0">
                                        <?php echo get_field('sto_g_cs_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('sto_g_cs_g_description')): ?>
                                    <div class="txt-def fs-20 mb-5">
                                        <?php echo get_field('sto_g_cs_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('sto_g_cs_g_link')) : $btn = get_field('sto_g_cs_g_link'); ?>
                                    <a class="btn-def"
                                        <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                       href="<?php echo $btn['url']; ?>"
                                    >
                                        <?php echo $btn['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
