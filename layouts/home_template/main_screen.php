<div class="section home-bg"
     data-anchor="<?php echo sanitize_title(get_sub_field('title_label')); ?>"
     <?php if (get_sub_field('background_image')): ?>
     style="background-image: url(<?php echo get_sub_field('background_image')['url']; ?>);"
     <?php endif ?>
>
    <div class="home-content h-100">

        <?php if (get_sub_field('title')) : ?>
            <div class="home-content__title">
                <?php echo get_sub_field('title'); ?>
            </div>
        <?php endif; ?>

        <?php if (get_sub_field('description')) : ?>
            <div class="home-content__text">
                <?php echo get_sub_field('description'); ?>
            </div>
        <?php endif; ?>

        <?php if (get_sub_field('button')) : $btn = get_sub_field('button'); ?>
            <a class="home-content__btn"
                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
               href="<?php echo $btn['url']; ?>"
            >
                <?php echo $btn['title']; ?>
            </a>
        <?php endif; ?>

        <?php if (have_rows('logo_r')) : ?>
            <ul class="home-content__logos">
                <?php while (have_rows('logo_r')) : the_row(); ?>
                    <?php $btn = get_sub_field('link') ? get_sub_field('link') : false; ?>
                    <li>
                        <?php if (get_sub_field('image')) : ?>
                            <img src="<?php echo get_sub_field('image')['url'] ?>" alt="<?php echo get_sub_field('image')['alt'] ?>">
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>