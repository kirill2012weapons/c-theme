<?php
/**
 * Template Name: AGB Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <div class="pt-140">
            <?php if (have_rows('ato_g_tc_r')): ?>
                <div class="container container_content pb-110">
                    <?php while (have_rows('ato_g_tc_r')): the_row(); ?>
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <?php if (get_sub_field('title')): ?>
                                    <div class="title-def">
                                        <?php echo get_sub_field('title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_sub_field('description')): ?>
                                    <p class="mt-4">
                                        <?php echo get_sub_field('description'); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <?php if (get_sub_field('sub_description')): ?>
                                <div class="col-md-6">
                                    <div class="light-grey-wrapper">
                                        <div class="txt-def">
                                            <?php echo get_sub_field('sub_description'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

            <div class="light-grey-wrapper light-grey-wrapper_paddingless pt-100 pb-110">
                <div class="container container_content">
                    <div class="row">
                        <div class="col-md-7">
                            <div>
                                <div class="md-subtitle">1. Fristen und Termine</div>
                                <div class="txt-def mt-3">Fristen gelten als nur annähernd vereinbart, es sei denn, dass die confiTECH ausdrücklich schriftlich Verbindlichkeit zusagt. Sie beginnen nicht zu laufen, bevor nicht vom Auftraggeber zu beschaffende Unterlagen, Genehmigungen und Freigaben beigebracht und fällige Anzahlungen geleistet sind.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">2. Arbeitssicherheit</div>
                                <div class="txt-def mt-3">Der Auftraggeber wird die eingesetzten Mitarbeiter der confiTECH über die bei ihm bestehenden besonderen Sicherheitsbestimmungen vor Beginn der Arbeiten unterrichten.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">3. Abnahme und Gefahrenübergang</div>
                                <div class="txt-def mt-3">Die Abnahme einer Leistung / Teilleistung hat innerhalb von 7 Tagen nach Zugang der Fertigstellungsanzeige zu erfolgen, sonst gilt die Leistung als abgenommen. Die Leistung gilt als abgenommen, wenn sie in Gebrauch genommen worden ist. Vorbehalte z.B. wegen bekannter Mängel oder etwaiger Vertragsstrafen, die nicht bei der Abnahme, mangels Abnahme vor Ingebrauchnahme gegenüber der confiTECH erklärt worden sind, verfallen. Bei Gewerken geht die Gefahr mit der Abnahme, mangels Abnahme mit der Ingebrauchnahme über, bei Käufen mit dem Versand der Ware.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">4. Gewährleistung</div>
                                <div class="txt-def mt-3">Zusicherung von Eigenschaften bedürfen der schriftlichen Bestätigung der confiTECH. Bei wesentlichen Mängeln ist die confiTECH berechtigt, zwei Nachbesserungen vorzunehmen. Wird eine Nachbesserung von der confiTECH abgelehnt oder schlägt auch die zweite Nachbesserung fehl, kann Minderung verlangt werden. Weitergehende Gewährleistungsansprüche sind ausgeschlossen. Gewährleistungsansprüche verjähren nach Maßgabe der gesetzlichen Bestimmungen.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">5. Preise und Zahlung</div>
                                <div class="txt-def mt-3">Kosten für Transport und Verpackung trägt der Auftraggeber. Rechnungen sind sofort und ohne Abzug zur Zahlung fällig. Bei Teilleistungen ist eine Schlußrechnung zu stellen unter Berücksichtigung sämtlicher Nachtragsaufträge und erhaltener Anzahlungen. Bei Zahlungsverzug kann ein Verzugszins von 8 % p.a. verlangt werden. Während des Zahlungsverzuges ist die confiTECH berechtigt, jede weitere Leistung zurückzuhalten. Für die Rechtzeitigkeit einer Zahlung ist maßgeblich der Geldeingang bei der confiTECH.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">6. Eigentumsvorbehalt</div>
                                <div class="txt-def mt-3">Alle Nutzungs- und Verwertungsrechte und das Eigentum gehen erst mit vollständiger Bezahlung des vereinbarten Preises auf den Auftraggeber über. Der Auftraggeber kann auch durch Verarbeitung des Liefergegenstandes zu einer neuen Sache kein Eigentum erwerben, erverarbeitet für die confiTECH, solange der Vorbehalt gilt.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">7. Haftung</div>
                                <div class="txt-def mt-3">Die Haftung der confiTECH für Organ-Verschulden und für Verschulden von Mitarbeitern und Erfüllungsgehilfen ist begrenzt auf Vorsatz und grobe Fahrlässigkeit. Die Haftung ist der Höhe nach begrenzt auf den Nettoauftragswert. Schadenersatzansprüche verjähren innerhalb vonsechs Monaten nach Empfang der Lieferung oder Leistung.</div>
                            </div>
                            <div class="mt-5">
                                <div class="md-subtitle">8. Erfüllungsort, Gerichtsstand, Recht</div>
                                <div class="txt-def mt-3">Für alle vertraglichen Beziehungen gilt deutsches Recht. Erfüllungsort für alle Zahlungen und Gerichtsstand ist der Sitz der Gesellschaft, Ulm (Donau). 02/2006</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
