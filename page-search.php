<?php
/**
 * Template Name: Search Page
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

<?php
$searchTarget = isset($_GET['target']) && !empty($_GET['target'])
    ? $_GET['target']
    : false;
?>

<main class="pt-140">
    <div class="container container_content">
        <?php get_search_form([
                'red' => true
        ]); ?>
        <?php if ($searchTarget): ?>
            <h3 class="page-title mt-50">
                <?php echo __( 'Search'); ?>
                <span>:"<?php echo $searchTarget; ?>"</span>
            </h3>
        <?php endif; ?>

        <?php
        $attrs = [
            's'         => $searchTarget,
            'post_type' => [
                'career_pt', 'page'
            ]
        ];

        $query = new WP_Query($attrs);
        ?>

        <?php if ($searchTarget): ?>
            <div class="mt-5 mb-150">
                <div class="d-flex flex-wrap">
                    <?php foreach ($query->posts as $post): ?>
                        <?php /** @var $post WP_Post */ ?>
                        <?php
                        global $wpdb;

                        $result = $wpdb->get_row(
                            sprintf(
                                "SELECT * FROM `wp_postmeta` WHERE `meta_value` LIKE '%s' AND `post_id` = %s",
                                '%' . $searchTarget . '%',
                                $post->ID
                            )
                        );
                        ?>
                        <a class="vacancy-item vacancy-item_full" href="<?php echo get_permalink($post->ID) ?>">
                            <p class="font-weight-bold">
                                <?php echo $post->post_title; ?>
                            </p>
                            <?php if (null !== $result): ?>
                                <?php
                                    $searchText = App\Services\TextWorker::catTextBySearchString(
                                        $result->meta_value,
                                        $searchTarget,
                                        110,
                                        110
                                    );
                                ?>
                                <?php if ($searchText): ?>
                                    <div class="d-flex align-items-center txt-dark-grey mb-2">
                                        <span>
                                            <?php if (strpos($searchText, '.php') !== false): ?>
                                                <?php echo $post->post_title; ?>
                                            <?php else: ?>
                                                <?php echo $searchText ?>
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <div class="d-flex align-items-center txt-dark-grey">
                                <span>
                                    <?php echo get_post_meta($post->ID, '_yoast_wpseo_metadesc', true) ?>
                                </span>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php else: ?>
            <?php if ($searchTarget): ?>
                <div class="mt-5 mb-150">
                    <div class="d-flex flex-wrap">
                        <p class="font-weight-bold">
                            <?php echo __( 'Post not found', 'sitepress' ) ?>
                        </p>
                    </div>
                </div>
            <?php else: ?>
                <div class="mt-5 mb-150">
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
