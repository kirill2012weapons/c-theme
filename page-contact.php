<?php
/**
 * Template Name: Contact Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php
use App\Model\ContactModel;
use App\Factory\FormFactory;
use App\Form\ContactFormType;
use Symfony\Component\HttpFoundation\Request;
use App\Services\FormManager as FM;

$contactModel = new ContactModel();
$contactForm  = FormFactory::create()
    ->createBuilder(
        ContactFormType::class,
        $contactModel
    )
    ->getForm()
    ->handleRequest(Request::createFromGlobals());

if ($contactForm->isSubmitted() && $contactForm->isValid()) {
    $mailManager = new App\Manager\MailManager();
    $mailManager->saveMail($contactModel);

    App\Services\Flash::init()
        ->message('success_contact', 'Vielen Dank für Ihre Anfrage. <br><br>Wir werden uns zeitnah bei Ihnen melden. <br>Ihr Confitech-Team.')
        ->redirectLocation($_SERVER['WP_HOME'] . '/' . $_SERVER['REQUEST_URI'])
        ->withStatus(302)
        ->redirect();
}

/** @var Symfony\Component\Form\FormView $contactFormView */
$contactFormView = $contactForm->createView();
?>

<?php get_header(); ?>

    <main class="pt-140 pb-110">
        <div class="container container_content">
            <div class="row">
                <div class="col-md-12 col-lg-6 mb-5 mb-lg-0">
                    <div class="position-relative map-block">
                        <?php if (get_field('cto_ci_g_image')) : ?>
                            <img src="<?php echo get_field('cto_ci_g_image')['url'] ?>" alt="<?php echo get_field('cto_ci_g_image')['alt'] ?>"/>
                        <?php else: ?>
                            <img src="<?php echo home_url('/wp-content/themes/confitech_html/build/img/bg-map.png'); ?>" alt="img"/>
                        <?php endif; ?>

                        <div class="address-block">
                            <?php if (get_field('cto_ci_g_title')) : ?>
                                <div class="address-block__title"><?php echo get_field('cto_ci_g_title'); ?></div>
                            <?php endif; ?>
                            <?php if (get_field('cto_ci_g_phone')) : ?>
                                <p class="d-flex align-items-center">
                                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.43228 7.63H6.37012C6.9224 7.63 7.37012 7.18229 7.37012 6.63V4.63C7.37012 3.52544 6.47469 2.63 5.37012 2.63H3.37012C2.81783 2.63 2.37012 3.07772 2.37012 3.63C2.37012 9.39831 7.60181 14.63 13.3701 14.63C13.9224 14.63 14.3701 14.1823 14.3701 13.63V11.63C14.3701 10.5254 13.4747 9.63 12.3701 9.63H10.3701C9.81783 9.63 9.37012 10.0777 9.37012 10.63V11.5678C7.82969 10.7274 6.50452 9.44595 5.60619 7.93516L5.43228 7.63ZM14.3701 7.63C14.3701 4.86858 12.1315 2.63 9.37012 2.63C8.81783 2.63 8.37012 3.07772 8.37012 3.63C8.37012 4.18229 8.81783 4.63 9.37012 4.63C10.9678 4.63 12.2738 5.87892 12.365 7.45373L12.3701 7.63C12.3701 8.18229 12.8178 8.63 13.3701 8.63C13.9224 8.63 14.3701 8.18229 14.3701 7.63Z" fill="#0D39BE"></path>
                                    </svg>
                                    <span><?php echo get_field('cto_ci_g_phone'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if (get_field('cto_ci_g_fax')) : ?>
                                <p class="d-flex align-items-center">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.1983 1.02783H3.80176V2.87697H11.1983V1.02783Z" fill="#0D39BE"></path>
                                        <path d="M14 3.77368H1V11.1702H2.84914V8.42454H12.0948V11.1702H14V3.77368ZM13.0474 5.62282H12.1509V4.72627H13.0474V5.62282Z" fill="#0D39BE"></path>
                                        <path d="M11.1983 9.3208H3.80176V13.9717H11.1983V9.3208Z" fill="#0D39BE"></path>
                                    </svg>
                                    <span><?php echo get_field('cto_ci_g_fax'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if (get_field('cto_ci_g_email')) : ?>
                                <p class="d-flex align-items-center">
                                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M14.3701 4.93001V12.63C14.3701 13.1429 13.9841 13.5655 13.4868 13.6233L13.3701 13.63H3.37013C2.8573 13.63 2.43462 13.244 2.37686 12.7466L2.37012 12.63V4.93001L7.71163 9.3826C8.08865 9.71249 8.65161 9.71249 9.02864 9.3826L14.3701 4.93001ZM12.8031 3.63L8.37013 7.30125L3.93612 3.63H12.8031Z" fill="#0D39BE"></path>
                                    </svg>
                                    <span><?php echo get_field('cto_ci_g_email'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if (get_field('cto_ci_g_address')) : ?>
                                <p class="d-flex align-items-center">
                                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.37012 2.63C11.1237 2.63 13.3701 5.00767 13.3701 7.95846C13.3701 9.67361 11.8835 11.8384 8.685 14.3826L8.37012 14.63C4.9542 11.9785 3.37012 9.72894 3.37012 7.95846C3.37012 5.00767 5.6165 2.63 8.37012 2.63ZM8.37012 5.63C7.26555 5.63 6.37012 6.52544 6.37012 7.63C6.37012 8.73457 7.26555 9.63 8.37012 9.63C9.47469 9.63 10.3701 8.73457 10.3701 7.63C10.3701 6.52544 9.47469 5.63 8.37012 5.63Z" fill="#0D39BE"></path>
                                    </svg>
                                    <span><?php echo get_field('cto_ci_g_address'); ?></span>
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?php if (App\Services\Flash::isMessage('success_contact')) : ?>
                    <div class="sm-subtitle mt-4 mb-4"><?php echo App\Services\Flash::getMessage('success_contact') ?></div>
                    <?php App\Services\Flash::cleanUpFlashMessages() ?>
                <?php else: ?>
                    <form class="col-md-12 col-lg-6" method="post" enctype="multipart/form-data">
                        <div class="title-def mb-4">Schreiben Sie uns</div>
                        <?php foreach (FM::getErrors($contactFormView, 'name') as $error) : ?>
                            <div class="error-wrapper">
                                <?php echo $error->getMessage(); ?>
                            </div>
                        <?php endforeach; ?>
                        <input class="input-def input-def_border mb-2 <?php if (FM::hasErrors($contactFormView, 'name')) : ?> error<?php endif; ?>"
                               type="text"
                               placeholder="Nahme"
                               name="<?php echo FM::name($contactFormView, 'name'); ?>"
                               value="<?php echo FM::value($contactFormView, 'name'); ?>"
                        />

                        <?php foreach (FM::getErrors($contactFormView, 'email') as $error) : ?>
                            <div class="error-wrapper">
                                <?php echo $error->getMessage(); ?>
                            </div>
                        <?php endforeach; ?>
                        <input class="input-def input-def_border mb-2 <?php if (FM::hasErrors($contactFormView, 'email')) : ?> error<?php endif; ?>"
                               type="text"
                               placeholder="E-mail"
                               name="<?php echo FM::name($contactFormView, 'email'); ?>"
                               value="<?php echo FM::value($contactFormView, 'email'); ?>"
                        />

                        <?php foreach (FM::getErrors($contactFormView, 'message') as $error) : ?>
                            <div class="error-wrapper">
                                <?php echo $error->getMessage(); ?>
                            </div>
                        <?php endforeach; ?>
                        <textarea class="textarea-def mb-2 <?php if (FM::hasErrors($contactFormView, 'message')) : ?> error<?php endif; ?>"
                                  placeholder="Nachricht"
                                  rows="50"
                                  name="<?php echo FM::name($contactFormView, 'message'); ?>"
                        ><?php echo FM::value($contactFormView, 'message'); ?></textarea>

                        <?php foreach (FM::getErrors($contactFormView, 'file') as $error) : ?>
                            <div class="error-wrapper">
                                <?php echo $error->getMessage(); ?>
                            </div>
                        <?php endforeach; ?>
                        <div class="upload-def mb-3 upload-def-js <?php if (FM::hasErrors($contactFormView, 'file')) : ?> error<?php endif; ?>">
                            <input type="file"
                                   id="file_upload"
                                   name="<?php echo FM::name($contactFormView, 'file'); ?>"
                            />
                            <div class="d-flex justify-content-between w-100"><span class="name-input-js">Datei anhängen</span><span>PNG, JPG, PDF</span></div>
                        </div>

                        <div class="mb-5 mb-lg-4">
                            <?php foreach (FM::getErrors($contactFormView, 'policy') as $error) : ?>
                                <div class="error-wrapper">
                                    <?php echo $error->getMessage(); ?>
                                </div>
                            <?php endforeach; ?>
                            <label class="cust-checkbox <?php if (FM::hasErrors($contactFormView, 'policy')) : ?> error-btm<?php endif; ?>">
                                <input type="checkbox"
                                       name="<?php echo FM::name($contactFormView, 'policy'); ?>"
                                />
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
                                    <g fill="none" fill-rule="evenodd">
                                        <rect width="13" height="13" fill="#FFF" rx="1"/>
                                        <path fill="#1C1D22" fill-rule="nonzero" d="M9.36 3.67l1.046 1.027L5.6 9.505 2.593 6.5 3.67 5.41 5.6 7.32z"/>
                                    </g>
                                </svg>
                                <?php if (get_field('cto_wtu_g_privacy_text')) : ?>
                                    <span>
                                        <?php echo get_field('cto_wtu_g_privacy_text'); ?>
                                    </span>
                                <?php else: ?>
                                    <span>
                                        I have read the Privacy Policy
                                    </span>
                                <?php endif; ?>
                            </label>
                        </div>
                        <div class="d-flex justify-content-center justify-content-lg-start">
                            <button class="btn-def" type="submit"><?php echo __('Send a message', 'WordPress') ?></button>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
