    <?php if (!is_404()): ?>
        <div class="footer">
            <div class="container container_footer">
                <?php echo get_template_part('template-parts/footer', 'main') ?>
            </div>
        </div>
        <div class="cookie-banner cookie-banner--hide" id="cookie-banner">
            <div class="mb-4">Wir möchten Ihnen den bestmöglichen Service bieten. Dazu speichern wir Informationen über Ihren Besuch in sogenannten Cookies. Durch die Nutzung dieser Webseite erklären Sie sich mit der Verwendung von Cookies einverstanden.</div><span class="cookie-banner-button" id="cookie-banner-button">OK</span><a class="cookie-banner-button" href="<?php echo home_url('/it-development-datenschutz/'); ?>">Datenschutz</a>
        </div>
    <?php endif; ?>
    <?php wp_footer(); ?>
</body>
</html>