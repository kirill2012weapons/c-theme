<?php
/**
 * Template Name: Mobility Solution Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <?php if (get_field('msto_g_ms_g_active')): ?>
            <div class="main-banner main-banner_mobility"
                <?php if (get_field('msto_g_ms_g_background_image')): ?>
                    style="background-image: url(<?php echo get_field('msto_g_ms_g_background_image')['url'] ?>);"
                <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center h-100">
                        <?php if (get_field('msto_g_ms_g_title')): ?>
                            <div class="title-def mb-5">
                                <?php echo get_field('msto_g_ms_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('msto_g_ms_g_description')): ?>
                            <p class="mw-40 mb-5 text-center">
                                <?php echo get_field('msto_g_ms_g_description'); ?>
                            </p>
                        <?php endif; ?>
                        <?php if (get_field('msto_g_ms_g_link')) : $btn = get_field('msto_g_ms_g_link'); ?>
                            <a class="btn-def"
                                <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                               href="<?php echo $btn['url']; ?>"
                            >
                                <?php echo $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('msto_g_content_r')): ?>
            <div class="pt-140 pb-110">
                <div class="container container_content">
                    <?php while (have_rows('msto_g_content_r')): the_row(); ?>
                        <div class="row align-items-center pb-110 <?php if (get_row_index() % 2 == 0): ?>flex-wrap-reverse flex-md-wrap<?php endif; ?>">
                            <?php if (get_row_index() % 2 == 0 && get_sub_field('image')): ?>
                                <div class="col-md-6 position-relative">
                                    <img class="solution-image"
                                         src="<?php echo get_sub_field('image')['sizes']['2048x2048']; ?>"
                                         alt="<?php echo get_sub_field('image')['title']; ?>"/>
                                </div>
                            <?php endif; ?>
                            <div class="<?php if (get_row_index() % 2 == 0): ?>col-md-6 col-lg-5 offset-lg-1 mb-4 mb-md-0<?php else: ?>col-md-6 col-lg-5 mb-4 mb-md-0 <?php endif; ?>">
                                <?php if (get_sub_field('title')): ?>
                                    <div class="title-def mb-4">
                                        <?php echo get_sub_field('title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_sub_field('content')): ?>
                                    <div class="txt-def">
                                        <?php echo get_sub_field('content'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if (get_row_index() % 2 != 0 && get_sub_field('image')): ?>
                                <div class="col-md-6 offset-lg-1 position-relative">
                                    <img class="solution-image"
                                         src="<?php echo get_sub_field('image')['sizes']['2048x2048']; ?>"
                                         alt="<?php echo get_sub_field('image')['title']; ?>"/>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>