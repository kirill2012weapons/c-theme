$(document).ready(function () {
    anchorsScroll();
    anchorsScrollFromAdminPanel();

    $('#search-click').on('click', function (event) {
        event.preventDefault();

        $('#nav-open-burger .nav-icon').click();
        $('#target').focus();
    })
});

function anchorsScroll() {
    $('[data-anchor]').on('click', function (event) {
        var anchor = $(event.target).attr('data-anchor');

        if (!anchor) {
            return;
        }
        event.preventDefault();

        $('html,body').animate({
            scrollTop: $('#' + anchor).offset().top - $('.header').height() - 8
        }, 'slow');
    })
}

function anchorsScrollFromAdminPanel() {
    const scrollTo = (searchStr, searchPosition) => {
        if (searchPosition >= 0) {
            const res = getFirstElementByText(decodeURI(searchStr.slice(searchPosition + 1)));

            if (res) {
                $('html,body').animate({
                    scrollTop: $(res).offset().top - $('.header').height() - 100
                }, 'slow');
            }
        }
    }

    if (location.href.trim().indexOf('#') > -1) {
        const searchPosition = location.href.trim().indexOf('#');
        scrollTo(location.href, searchPosition);
    }

    $('a.dynamic-link[href]').on('click', function (event) {
        if ($(this).attr('href').trim().indexOf('#') > -1) {
            event.preventDefault();

            const searchPosition = $(this).attr('href').trim().indexOf('#');
            scrollTo($(this).attr('href'), searchPosition);

            location.href = $(this).attr('href').trim();
        }

        $('.nav-icon').removeClass('open');
        $('.menu-drop').removeClass('open');
        $('.header__logo').removeClass('black');
        $("body").removeClass('overflow-hidden')
    })
}

function getFirstElementByText(str) {
    let results = [];
    const documentInner = document;

    const searchElements = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'a', 'span', 'b', 'strong', 'div'];

    searchElements.forEach(searchElement => {
        if (results.length === 0) {
            results = Array.from(documentInner.getElementsByTagName(searchElement)).filter(el => ~el.textContent.trim().indexOf(str));
        }
    })

    if (results.length === 0) return false
    else return results[results.length - 1];
}