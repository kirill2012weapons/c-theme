<?php

namespace App\WpEnqueue;

interface EnqueueInterface
{
    /**
     * @param EnqueueDTO $enqueueDTO
     *
     * @return $this
     */
    public function add(EnqueueDTO $enqueueDTO);

    /**
     * @return static
     */
    static public function init();

    public function enqueue();
}