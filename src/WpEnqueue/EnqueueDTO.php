<?php

namespace App\WpEnqueue;

class EnqueueDTO
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $depth;

    public function __construct(string $slug, string $path, array $depth = [])
    {
        $this->slug  = $slug;
        $this->path  = $path;
        $this->depth = $depth;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getDepth(): array
    {
        return $this->depth;
    }
}