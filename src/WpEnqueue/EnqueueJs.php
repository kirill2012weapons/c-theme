<?php

namespace App\WpEnqueue;

use App\Git\GitVersion;

class EnqueueJs implements EnqueueInterface
{
    /**
     * @var EnqueueDTO[]
     */
    static private $scripts = [];

    /**
     * @var EnqueueDTO[]
     */
    static private $scriptsCalculator = [];

    /**
     * @return EnqueueInterface|EnqueueJs
     */
    static public function init()
    {
        $self = new self();

        add_action('wp_enqueue_scripts', [$self, 'enqueue']);

        return $self;
    }

    /**
     * @param EnqueueDTO $enqueueDTO
     *
     * @return $this|EnqueueInterface
     */
    public function add(EnqueueDTO $enqueueDTO)
    {
        self::$scripts[] = $enqueueDTO;

        return $this;
    }

    /**
     * @param EnqueueDTO $enqueueDTO
     *
     * @return $this|EnqueueInterface
     */
    public function addCalculatorScript(EnqueueDTO $enqueueDTO)
    {
        self::$scriptsCalculator[] = $enqueueDTO;

        return $this;
    }

    public function enqueue(): void
    {
        array_map(function (EnqueueDTO $enqueueDTO) {
            wp_enqueue_script(
                $enqueueDTO->getSlug(),
                $enqueueDTO->getPath(),
                $enqueueDTO->getDepth(),
                GitVersion::version(),
                true
            );
        }, self::$scripts);

        if (is_page_template('page-calculator.php')) {
            array_map(function (EnqueueDTO $enqueueDTO) {
                wp_enqueue_script(
                    $enqueueDTO->getSlug(),
                    $enqueueDTO->getPath(),
                    $enqueueDTO->getDepth(),
                    GitVersion::version(),
                    true
                );
            }, self::$scriptsCalculator);
        }
    }
}