<?php

namespace App\WpEnqueue;

use App\Git\GitVersion;

class EnqueueCss implements EnqueueInterface
{
    /**
     * @var EnqueueDTO[]
     */
    static private $styles = [];

    /**
     * @var EnqueueDTO[]
     */
    static private $stylesAdmin = [];

    /**
     * @return EnqueueCss
     */
    static public function init()
    {
        $self = new self();

        add_action('wp_enqueue_scripts', [$self, 'enqueue']);
        add_action('admin_enqueue_scripts', [$self, 'enqueueAdmin']);

        return $self;
    }

    /**
     * @param EnqueueDTO $enqueueDTO
     *
     * @return $this|EnqueueInterface
     */
    public function add(EnqueueDTO $enqueueDTO)
    {
        self::$styles[] = $enqueueDTO;

        return $this;
    }

    public function addAdmin(EnqueueDTO $enqueueDTO)
    {
        self::$stylesAdmin[] = $enqueueDTO;

        return $this;
    }

    public function enqueueAdmin(): void
    {
        array_map(function (EnqueueDTO $enqueueDTO){
            wp_enqueue_style(
                $enqueueDTO->getSlug(),
                $enqueueDTO->getPath(),
                $enqueueDTO->getDepth(),
                GitVersion::version(),
                'all'
            );
        }, self::$stylesAdmin);
    }

    public function enqueue(): void
    {
        array_map(function (EnqueueDTO $enqueueDTO){
            wp_enqueue_style(
                $enqueueDTO->getSlug(),
                $enqueueDTO->getPath(),
                $enqueueDTO->getDepth(),
                GitVersion::version(),
                'all'
            );
        }, self::$styles);
    }
}