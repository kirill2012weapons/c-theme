<?php

namespace App\Factory;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorFactory
{
    /**
     *          | Create Validation Factory
     *
     * @return ValidatorInterface
     */
    static public function create()
    {
        return Validation::createValidator();
    }
}