<?php

namespace App\Factory;

use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;

class FormFactory
{
    /**
     *          | Create Form Factory with
     *          |       HttpFoundationExtension and ValidationExtension
     *
     * @return FormFactoryInterface
     */
    static public function create()
    {
        return (Forms::createFormFactoryBuilder())
            ->addExtension(new HttpFoundationExtension())
            ->addExtension(new ValidatorExtension(ValidatorFactory::create()))
            ->getFormFactory();
    }
}