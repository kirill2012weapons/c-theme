<?php

namespace App\WPMLSwitcher;

class WPMLSwitcher
{
    static public function getSwitcher()
    {
        $languages = apply_filters( 'wpml_active_languages', null, [] );

        $outPut = '<div class="language-block">';

        foreach ($languages as $lang => $data) {
            $outPut .= sprintf(
                '<a %s href="%s" style="margin-right: 15px;">' .
                '    <img class="mr-2" src="%s" alt="img"/>' .
                '    <span>%s</span>' .
                '</a>',
                $data['active'] ? 'class="active"' : '',
                $data['url'],
                $data['country_flag_url'],
                $data['native_name']
            );
        }

        return $outPut . '</div>';
    }
}