<?php

namespace App\Services;

use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormView;

class FormManager
{
    /**
     * @param FormView $view
     * @param string $fieldName
     *
     * @return mixed|string
     */
    static public function name(FormView $view, string $fieldName)
    {
        return $view->offsetGet($fieldName)
            ? $view->offsetGet($fieldName)->vars['full_name']
            : '';
    }

    /**
     * @param FormView $view
     * @param string $fieldName
     *
     * @return mixed|string
     */
    static public function value(FormView $view, string $fieldName)
    {
        return $view->offsetGet($fieldName)
            ? $view->offsetGet($fieldName)->vars['value']
            : '';
    }

    /**
     * @param FormView $view
     * @param string $fieldName
     *
     * @return int
     */
    static public function hasErrors(FormView $view, string $fieldName)
    {
        return $view->offsetGet($fieldName)
            ? $view->offsetGet($fieldName)->vars['errors']->count()
            : 0;
    }

    /**
     * @param FormView $view
     * @param string $fieldName
     *
     * @return FormErrorIterator|array
     */
    static public function getErrors(FormView $view, string $fieldName)
    {
        return $view->offsetGet($fieldName)
            ? $view->offsetGet($fieldName)->vars['errors']
            : [];
    }
}