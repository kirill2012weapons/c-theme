<?php

namespace App\Services\JMS;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

class Serializer
{
    /**
     * @param object $object
     * @param array $context
     * @param string $format
     *
     * @return string
     */
    static public function serialize(object $object, $context = [], $format = 'json')
    {
        $context = SerializationContext::create()
            ->setGroups($context);

        $serializer = SerializerBuilder::create()
            ->setSerializationContextFactory(function () use ($context) {
                return $context;
            })
            ->build();

        return $serializer->serialize($object, $format, $context);
    }
}