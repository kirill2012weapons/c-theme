<?php

namespace App\Services\REST;

interface HandlerInterface
{
    public function handle();
}