<?php

namespace App\Services\REST;

use Symfony\Component\HttpFoundation\Request;

class AjaxHandler
{
    /**
     * @var array
     */
    private $routes = [];

    static public function init()
    {
        return new self();
    }

    public function register()
    {
        add_action( 'wp_enqueue_scripts', function () {
            wp_localize_script( 'jquery_3_5_1_js', 'ajaxdata', [
                'url' => admin_url('admin-ajax.php')
            ]);
        }, 99 );

        foreach ($this->routes as $routeKey => $routeValue) {
            add_action('wp_ajax_' . $routeKey, [$routeValue, 'handle']);
            add_action('wp_ajax_nopriv_' . $routeKey, [$routeValue, 'handle']);
        }
    }

    /**
     * @param string $name
     * @param string $className
     * @return AjaxHandler
     *
     * @throws \Exception
     */
    public function route(string $name, string $className)
    {
        if (array_key_exists($name, $this->routes)) {
            throw new \Exception('Route is exist');
        }

        $this->routes[$name] = $className;

        return $this;
    }
}