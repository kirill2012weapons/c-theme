<?php

namespace App\Services\REST\Handlers;

use App\Repository\Calculator\CalculatorAppRepository;
use App\Services\REST\HandlerInterface;
use App\Services\JMS\Serializer;

/**
 * Class GetAppCalculatorInformationHandler
 *
 * @package App\Services\REST\Handlers
 */
class GetAppCalculatorInformationHandler implements HandlerInterface
{
    public function handle()
    {
        $repository = new CalculatorAppRepository();

        echo Serializer::serialize($repository->getStepContainer(), [
            'main_information', 'step'
        ]);
        die();
    }
}