<?php

namespace App\Services\REST\Handlers;

use App\Repository\Calculator\CalculatorTeamAppRepository;
use App\Services\REST\HandlerInterface;
use App\Services\JMS\Serializer;

/**
 * Class GetAppTeamCalculatorInformationHandler
 *
 * @package App\Services\REST\Handlers
 */
class GetAppTeamCalculatorInformationHandler implements HandlerInterface
{
    public function handle()
    {
        $repository = new CalculatorTeamAppRepository();

        echo Serializer::serialize($repository->getCalculatorContainer(), [
            'main_information', 'team'
        ]);
        die();
    }
}