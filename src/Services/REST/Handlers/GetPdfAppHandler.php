<?php

namespace App\Services\REST\Handlers;

use App\Model\ContactCalculator\ContactCalculatorModel;
use Symfony\Component\HttpFoundation\Request;
use App\Services\Generator\PdfGenerator;
use App\Services\REST\HandlerInterface;
use App\Form\ContactCalculatorType;
use App\Factory\FormFactory;

class GetPdfAppHandler implements HandlerInterface
{
    /**
     * @throws \Mpdf\MpdfException
     */
    public function handle()
    {
        $request         = Request::createFromGlobals();
        $instance        = new ContactCalculatorModel();

        $form = FormFactory::create()
            ->createBuilder(ContactCalculatorType::class, $instance, ['only_pdf' => true])
            ->getForm()
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $url = (new PdfGenerator())
                ->getPdfApp([
                    'model' => $instance
                ]);
            echo $url['url'];
            exit();
        } else {
            http_response_code(400);
            exit();
        }
    }
}