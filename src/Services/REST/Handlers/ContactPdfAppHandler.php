<?php

namespace App\Services\REST\Handlers;

use App\Model\ContactCalculator\ContactCalculatorModel;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use App\Services\Generator\PdfGenerator;
use App\Services\REST\HandlerInterface;
use App\Form\ContactCalculatorType;
use App\Factory\FormFactory;
use App\Manager\MailManager;
use App\Model\ContactModel;

class ContactPdfAppHandler implements HandlerInterface
{
    /**
     * @throws \Mpdf\MpdfException
     */
    public function handle()
    {
        $request         = Request::createFromGlobals();
        $instance        = new ContactCalculatorModel();

        $form = FormFactory::create()
            ->createBuilder(ContactCalculatorType::class, $instance)
            ->getForm()
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $contactModel = (new ContactModel())
                ->setName($instance->getName())
                ->setEmail($instance->getEmail())
                ->setMessage($instance->getMessage());

            $url = (new PdfGenerator())
                ->getPdfApp([
                    'model' => $instance
                ]);

            $file = new File($url['file_url']);
            if ($file) {
                $contactModel->setFile($file);
            }

            $mailManager = new MailManager();

            $mailManager->saveMail($contactModel, 'mails_calculator_pt', 'Order - ' . $url['order_id']);

            echo true;
            exit();
        } else {
            http_response_code(400);
            exit();
        }
    }
}