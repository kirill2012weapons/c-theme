<?php

namespace App\Services;

class Flash
{
    const FLASH_KEYS     = 'flash_message_stored_keys';
    const SKIP_FLAG      = 'skip_flash_clean_up';
    private $xRedirectBy = 'flash';
    private $statusCode  = 302;
    private $redirectUrl;

    public static function init()
    {
        if (!defined('FLASH_INIT')) {
            register_shutdown_function([Flash::class, 'cleanUpFlashMessages']);
            define('FLASH_INIT', true);
        }

        return new self();
    }

    public static function cleanUpFlashMessages()
    {
        if (!defined(self::SKIP_FLAG)) {
            if (isset($_SESSION[self::FLASH_KEYS])) {
                foreach ($_SESSION[self::FLASH_KEYS] as $message_key) {
                    unset($_SESSION[$message_key]);
                }

                unset($_SESSION[self::FLASH_KEYS]);
            }
        }
    }

    public function message($key, $message)
    {
        $_SESSION[self::FLASH_KEYS][] = $key;
        $_SESSION[$key]               = $message;

        if (!defined(self::SKIP_FLAG)) {
            define(self::SKIP_FLAG, true);
        }

        return $this;
    }

    public function redirectLocation($url)
    {
        $this->redirectUrl = $url;

        return $this;
    }

    public function withStatus($status = 302)
    {
        $this->statusCode = $status;

        return $this;
    }

    public function redirectBy($xRedirectBy = 'flash')
    {
        $this->xRedirectBy = $xRedirectBy;

        return $this;
    }

    public function redirect()
    {
        if (!isset($this->redirectUrl)) {
            $this->redirectBack();

            return;
        }

        @header("X-Redirect-By: $this->xRedirectBy", true, $this->statusCode);
        @header("Location: $this->redirectUrl", true, $this->statusCode);
        exit();
    }

    public function redirectBack()
    {
        $this->redirectUrl = $_SERVER['HTTP_REFERER'];
        $this->redirect();
    }

    public static function getMessage($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return false;
    }
    public static function isMessage($key)
    {
        if (isset($_SESSION[$key])) {
            return true;
        }

        return false;
    }

}