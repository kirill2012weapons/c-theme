<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class FileUploader
{
    static public function upload(UploadedFile $uploadedFile, $uploadFolder = '', $postId = 0)
    {
        $wordpressUploadDir = wp_upload_dir();

        $filePath = $wordpressUploadDir['basedir'] . '/'  . $uploadFolder . '/';
        $mimeType = $uploadedFile->getMimeType();

        try {
            /** @var File $file */
            $file = $uploadedFile->move(
                $filePath,
                (new \DateTime())->format('YmnHis') . '_' . $uploadedFile->getClientOriginalName()
            );
        } catch (FileException $exception) {
            return false;
        }


        $uploadId = wp_insert_attachment([
            'guid'           => $wordpressUploadDir['baseurl'] . '/'  . $uploadFolder . '/' . $file->getFilename(),
            'post_mime_type' => $mimeType,
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $file->getFilename()),
            'post_content'   => '',
            'post_status'    => 'inherit'
        ], $wordpressUploadDir['baseurl'] . '/'  . $uploadFolder . '/' . $file->getFilename(), $postId );

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            wp_update_attachment_metadata( $uploadId, wp_generate_attachment_metadata( $uploadId, $mimeType ) );

            return $uploadId;

    }

    static public function uploadFile(File $uploadedFile, $uploadFolder = '', $postId = 0)
    {
        $wordpressUploadDir = wp_upload_dir();

        $filePath = $wordpressUploadDir['basedir'] . '/'  . $uploadFolder . '/';
        $mimeType = $uploadedFile->getMimeType();

        try {
            /** @var File $file */
            $file = $uploadedFile->move(
                $filePath,
                (new \DateTime())->format('YmnHis') . '_' . $uploadedFile->getFilename()
            );
        } catch (FileException $exception) {
            return false;
        }

        $uploadId = wp_insert_attachment([
            'guid'           => $wordpressUploadDir['baseurl'] . '/'  . $uploadFolder . '/' . $file->getFilename(),
            'post_mime_type' => $mimeType,
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $file->getFilename()),
            'post_content'   => '',
            'post_status'    => 'inherit'
        ], $wordpressUploadDir['baseurl'] . '/'  . $uploadFolder . '/' . $file->getFilename(), $postId );

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            wp_update_attachment_metadata( $uploadId, wp_generate_attachment_metadata( $uploadId, $mimeType ) );

            return $uploadId;

    }
}