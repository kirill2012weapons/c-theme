<?php

namespace App\Services;

class TextWorker
{
    static public function catTextBySearchString($text, $searchWord, $symbolsBefore = 100, $symbolsAfter = 100)
    {
        $newString = wp_strip_all_tags($text, true);

        preg_match(
            '/(.){0,' . $symbolsBefore . '}(' . $searchWord . ')(.){0,' . $symbolsAfter . '}/i',
            $newString,
            $matches
        );

        if (!empty($matches) && $matches[0]) {
            return '...' . str_replace($searchWord, '<span style="background-color: yellow;">' . $searchWord . '</span>', $matches[0]) . '...';
        }

        return false;
    }
}