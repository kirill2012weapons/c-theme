<?php

namespace App\Services\Generator;

use Mpdf\Output\Destination;
use Mpdf\Mpdf;

class PdfGenerator
{
    public const UPLOAD_PATH = ABSPATH . 'wp-content/uploads/pdf';

    /**
     * Mpdf\Output\Destination const
     *
     * @param string $typeOfView
     * @param array $args
     *
     * @return array
     * @throws \Mpdf\MpdfException
     */
    public function getPdfTeam(
        $args
    ) {
        $mpdf = new Mpdf();

        $randomString     = (new RandomStringGenerator())->generate(5) . '-' . date('d');
        $args['order_id'] = $randomString;

        ob_start();
        get_template_part('template-parts/pdf', 'team', $args);
        $output =  ob_get_clean();

        $mpdf->WriteHTML(
            $output
        );

        $uploadPath   = self::UPLOAD_PATH . '/Angebot personelle Unterstützung Confitech GmbH ' . $randomString . '.pdf';

        $mpdf->Output(
            $uploadPath,
            Destination::FILE
        );

        return [
            'url'      => home_url() . '/wp-content/uploads/pdf/Angebot personelle Unterstützung Confitech GmbH ' . $randomString . '.pdf',
            'file_url' => $uploadPath,
            'order_id' => $randomString
        ];
    }

    /**
     * Mpdf\Output\Destination const
     *
     * @param string $typeOfView
     * @param array $args
     *
     * @return array
     * @throws \Mpdf\MpdfException
     */
    public function getPdfApp(
        $args
    ) {
        $mpdf = new Mpdf();

        $randomString     = (new RandomStringGenerator())->generate(5) . '-' . date('d');
        $args['order_id'] = $randomString;

        ob_start();
        get_template_part('template-parts/pdf', 'app', $args);
        $output =  ob_get_clean();

        $mpdf->WriteHTML(
            $output
        );

        $uploadPath   = self::UPLOAD_PATH . '/Angebot Web-App-Entwicklung Confitech GmbH ' . $randomString . '.pdf';

        $mpdf->Output(
            $uploadPath,
            Destination::FILE
        );

        return [
            'url'      => home_url() . '/wp-content/uploads/pdf/Angebot Web-App-Entwicklung Confitech GmbH ' . $randomString . '.pdf',
            'file_url' => $uploadPath,
            'order_id' => $randomString
        ];
    }
}