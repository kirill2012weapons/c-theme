<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Model\ContactCalculator\ContactCalculatorModel;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class ContactCalculatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['only_pdf']) {
            $builder
                ->add('name', TextType::class, [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Name is required.'
                        ])
                    ]
                ])
                ->add('email', EmailType::class, [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Email is required.'
                        ])
                    ]
                ])
                ->add('message', TextareaType::class)
            ;
        }

        $builder
            ->add('rows', CollectionType::class, [
                'entry_type'    => RowInformationType::class,
                'allow_add' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => ContactCalculatorModel::class,
            'only_pdf'           => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}