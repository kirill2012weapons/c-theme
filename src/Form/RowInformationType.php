<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use App\Model\ContactCalculator\RowInformation;
use Symfony\Component\Form\AbstractType;

class RowInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('elements', TextareaType::class)
            ->add('sum', NumberType::class)

            ->add('days', IntegerType::class)
            ->add('experience', TextType::class)
            ->add('position', TextType::class)
            ->add('price', NumberType::class)
            ->add('weeks', IntegerType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RowInformation::class
        ]);
    }
}