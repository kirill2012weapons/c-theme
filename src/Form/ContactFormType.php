<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use App\Model\ContactModel;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pflichtfeld'
                    ])
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pflichtfeld'
                    ]),
                    new Email([
                        'message' => 'E-Mail'
                    ])
                ]
            ])
            ->add('message', TextareaType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Nachricht'
                    ])
                ]
            ])
            ->add('file', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize'          => '2048k',
                        'mimeTypes'        => [
                            'application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'
                        ],
                        'maxSizeMessage'   => 'Diese Datei ist zu Groß ({{ size }} {{ suffix }}). Die maximale Größe einer Datei beträgt {{ limit }} {{ suffix }}.',
                        'mimeTypesMessage' => 'Der MIME-Typ der Datei ist ungültig ({{ type }}). Erlaubte MIME-Typen sind {{ types }}.'
                    ])
                ]
            ])
            ->add('policy', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pflichtfeld'
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactModel::class
        ]);
    }
}
