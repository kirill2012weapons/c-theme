<?php

namespace App\Model\ContactCalculator;

class RowInformation
{
    /**
     * @var mixed
     */
    protected $title;

    /**
     * @var mixed
     */
    protected $elements;

    /**
     * @var mixed
     */
    protected $sum;

    /**
     * @var integer|null|mixed
     */
    protected $days;

    /**
     * @var string|null|mixed
     */
    protected $experience;

    /**
     * @var string|null|mixed
     */
    protected $position;

    /**
     * @var integer|null|mixed
     */
    protected $price;

    /**
     * @var integer|null|mixed
     */
    protected $weeks;

    /**
     * @return mixed
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param mixed $elements
     *
     * @return self
     */
    public function setElements($elements): self
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * @return int|mixed|null
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param int|mixed|null $days
     *
     * @return self
     */
    public function setDays($days): self
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @return mixed|string|null
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed|string|null $experience
     *
     * @return self
     */
    public function setExperience($experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return mixed|string|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed|string|null $position
     *
     * @return self
     */
    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return int|mixed|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int|mixed|null $price
     *
     * @return self
     */
    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int|mixed|null
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    /**
     * @param int|mixed|null $weeks
     *
     * @return self
     */
    public function setWeeks($weeks): self
    {
        $this->weeks = $weeks;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param mixed $sum
     *
     * @return self
     */
    public function setSum($sum): self
    {
        $this->sum = $sum;

        return $this;
    }
}