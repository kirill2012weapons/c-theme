<?php

namespace App\Model\ContactCalculator;

class ContactCalculatorModel
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var RowInformation[]
     */
    protected $rows = [];

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return RowInformation[]
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * @param RowInformation[] $rows
     *
     * @return self
     */
    public function setRows(array $rows): self
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * @param RowInformation $row
     *
     * @return self
     */
    public function addRow(RowInformation $row): self
    {
        $this->rows[] = $row;

        return $this;
    }
}