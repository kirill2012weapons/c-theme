<?php

namespace App\Model\Calculator\Team\Main;

use App\Model\Calculator\App\Main\StepCalculator;
use JMS\Serializer\Annotation as JMS;

class CalculatorContainer
{
    /**
     * @var null|StepCalculator
     *
     * @JMS\Groups({"main_information"})
     */
    private $stepCalculator;

    /**
     * @var Specialist[]
     *
     * @JMS\Groups({"team"})
     */
    private $specialists;

    /**
     * @var Experience[]
     *
     * @JMS\Groups({"team"})
     */
    private $experiences;

    /**
     * @return StepCalculator|null
     */
    public function getStepCalculator(): ?StepCalculator
    {
        return $this->stepCalculator;
    }

    /**
     * @param StepCalculator|null $stepCalculator
     *
     * @return self
     */
    public function setStepCalculator(?StepCalculator $stepCalculator): self
    {
        $this->stepCalculator = $stepCalculator;

        return $this;
    }

    /**
     * @return Specialist[]
     */
    public function getSpecialists(): array
    {
        return $this->specialists;
    }

    /**
     * @param Specialist[] $specialists
     *
     * @return self
     */
    public function setSpecialists(array $specialists): self
    {
        $this->specialists = $specialists;

        return $this;
    }

    /**
     * @return Experience[]
     */
    public function getExperiences(): array
    {
        return $this->experiences;
    }

    /**
     * @param Experience[] $experiences
     *
     * @return self
     */
    public function setExperiences(array $experiences): self
    {
        $this->experiences = $experiences;

        return $this;
    }
}