<?php

namespace App\Model\Calculator\Team\Main;

use JMS\Serializer\Annotation as JMS;

class Specialist
{
    /**
     * @var null|string
     *
     * @JMS\Groups({"team"})
     */
    protected $id;

    /**
     * @var null|string
     *
     * @JMS\Groups({"team"})
     */
    protected $name;

    /**
     * @var null|float|integer
     *
     * @JMS\Groups({"team"})
     */
    protected $price;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return self
     */
    public function setId(?string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float|int|null
     */
    public function getPrice()
    {
        return (int)$this->price;
    }

    /**
     * @param float|int|null $price
     *
     * @return self
     */
    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }
}