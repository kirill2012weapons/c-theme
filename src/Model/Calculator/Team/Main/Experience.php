<?php

namespace App\Model\Calculator\Team\Main;

use JMS\Serializer\Annotation as JMS;

class Experience
{
    /**
     * @var null|string
     *
     * @JMS\Groups({"team"})
     */
    protected $id;

    /**
     * @var null|string
     *
     * @JMS\Groups({"team"})
     */
    protected $name;

    /**
     * @var null|float|integer
     */
    protected $innerPercent;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return self
     */
    public function setId(?string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float|int|null
     */
    public function getInnerPercent()
    {
        return $this->innerPercent;
    }

    /**
     * @param float|int|null $innerPercent
     *
     * @return self
     */
    public function setInnerPercent($innerPercent): self
    {
        $this->innerPercent = $innerPercent;

        return $this;
    }

    /**
     * @JMS\Groups({"team"})
     * @JMS\VirtualProperty
     * @JMS\SerializedName("percents")
     */
    public function getPercents()
    {
        return $this->innerPercent / 100;
    }
}