<?php

namespace App\Model\Calculator\App\Main;

use JMS\Serializer\Annotation as JMS;

class Choice
{
    /**
     * @var string
     *
     * @JMS\Groups({"step"})
     */
    protected $id;

    /**
     * @var string|null
     *
     * @JMS\Groups({"step"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @JMS\Groups({"step"})
     */
    private $description;

    /**
     * @var integer|null|string
     */
    private $hours;

    /**
     * @var integer|null|string
     */
    private $percents;

    /**
     * @var integer|null|string
     */
    private $pricePerHour;

    /**
     * @JMS\Groups({"step"})
     * @JMS\VirtualProperty
     * @JMS\SerializedName("sum")
     */
    public function getSum()
    {
        return $this->pricePerHour * $this->hours;
    }

    /**
     * @JMS\Groups({"step"})
     * @JMS\VirtualProperty
     * @JMS\SerializedName("percents")
     */
    public function getPercentsJsm()
    {
        return $this->percents / 100;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|string|null
     */
    public function getHours()
    {
        return (int) $this->hours;
    }

    /**
     * @param int|string|null $hours
     *
     * @return self
     */
    public function setHours($hours): self
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * @return int|string|null
     */
    public function getPercents()
    {
        return (int) $this->percents;
    }

    /**
     * @param int|string|null $percents
     *
     * @return self
     */
    public function setPercents($percents): self
    {
        $this->percents = $percents;

        return $this;
    }

    /**
     * @return int|string|null
     */
    public function getPricePerHour()
    {
        return (int) $this->pricePerHour;
    }

    /**
     * @param int|string|null $pricePerHour
     *
     * @return self
     */
    public function setPricePerHour($pricePerHour): self
    {
        $this->pricePerHour = $pricePerHour;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }
}