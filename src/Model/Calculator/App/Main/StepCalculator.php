<?php

namespace App\Model\Calculator\App\Main;

use JMS\Serializer\Annotation as JMS;

class StepCalculator
{
    /**
     * @var string|null
     *
     * @JMS\Groups({"main_information"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @JMS\Groups({"main_information"})
     */
    private $image;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     *
     * @return self
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}