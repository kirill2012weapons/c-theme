<?php

namespace App\Model\Calculator\App\Main;

use JMS\Serializer\Annotation as JMS;

class StepContainer
{
    /**
     * @var null|StepCalculator
     *
     * @JMS\Groups({"main_information"})
     */
    private $stepCalculator;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step1;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step2;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step3;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step4;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step5;

    /**
     * @var null|Step
     *
     * @JMS\Groups({"step"})
     */
    private $step6;

    /**
     * @return StepCalculator|null
     */
    public function getStepCalculator(): ?StepCalculator
    {
        return $this->stepCalculator;
    }

    /**
     * @param StepCalculator|null $stepCalculator
     *
     * @return self
     */
    public function setStepCalculator(?StepCalculator $stepCalculator): self
    {
        $this->stepCalculator = $stepCalculator;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep1(): ?Step
    {
        return $this->step1;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep1(?Step $step): self
    {
        $this->step1 = $step;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep2(): ?Step
    {
        return $this->step2;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep2(?Step $step): self
    {
        $this->step2 = $step;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep3(): ?Step
    {
        return $this->step3;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep3(?Step $step): self
    {
        $this->step3 = $step;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep4(): ?Step
    {
        return $this->step4;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep4(?Step $step): self
    {
        $this->step4 = $step;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep5(): ?Step
    {
        return $this->step5;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep5(?Step $step): self
    {
        $this->step5 = $step;

        return $this;
    }

    /**
     * @return Step|null
     */
    public function getStep6(): ?Step
    {
        return $this->step6;
    }

    /**
     * @param Step|null $step
     *
     * @return self
     */
    public function setStep6(?Step $step): self
    {
        $this->step6 = $step;

        return $this;
    }
}