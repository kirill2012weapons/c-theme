<?php

namespace App\Model\Calculator\App\Main;

use JMS\Serializer\Annotation as JMS;

class Step
{
    /**
     * @var array|ChoiceRepeater[]|null
     *
     * @JMS\Groups({"step"})
     */
    private $data = [];

    /**
     * @return ChoiceRepeater[]|array|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param ChoiceRepeater[]|array|null $data
     *
     * @return self
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param ChoiceRepeater|null $data
     *
     * @return self
     */
    public function addData($data): self
    {
        $this->data[] = $data;

        return $this;
    }
}