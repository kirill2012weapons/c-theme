<?php

namespace App\Model\Calculator\App\Main;

use JMS\Serializer\Annotation as JMS;

class ChoiceRepeater
{
    /**
     * @var array
     *
     * @JMS\Groups({"step"})
     */
    private $valuePicked = [];

    /**
     * @var string|null
     *
     * @JMS\Groups({"step"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @JMS\Groups({"step"})
     */
    private $shortTitle;

    /**
     * @var boolean
     *
     * @JMS\Groups({"step"})
     */
    private $multiple = false;

    /**
     * @var array|Choice[]|null
     *
     * @JMS\Groups({"step"})
     */
    private $choices;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Choice[]|array|null
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * @param Choice[]|array|null $choices
     *
     * @return self
     */
    public function setChoices($choices): self
    {
        $this->choices = $choices;

        return $this;
    }

    /**
     * @param Choice|null $choice
     *
     * @return self
     */
    public function addChoice($choice): self
    {
        $this->choices[] = $choice;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortTitle(): ?string
    {
        return $this->shortTitle;
    }

    /**
     * @param string|null $shortTitle
     *
     * @return self
     */
    public function setShortTitle(?string $shortTitle): self
    {
        $this->shortTitle = $shortTitle;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    /**
     * @param bool $multiple
     *
     * @return self
     */
    public function setMultiple(bool $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return array
     */
    public function getValuePicked(): array
    {
        return $this->valuePicked;
    }

    /**
     * @param array $valuePicked
     *
     * @return self
     */
    public function setValuePicked(array $valuePicked): self
    {
        $this->valuePicked = $valuePicked;

        return $this;
    }
}
