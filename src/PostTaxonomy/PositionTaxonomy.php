<?php

namespace App\PostTaxonomy;

class PositionTaxonomy
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public static function init()
    {
        return new self();
    }

    public function register()
    {
            $labels = [
                "name"          => __( "Positions", "custom-post-type-ui" ),
                "singular_name" => __( "Position", "custom-post-type-ui" ),
            ];

            $args = [
                "label"                 => __( "Positions", "custom-post-type-ui" ),
                "labels"                => $labels,
                "public"                => true,
                "publicly_queryable"    => true,
                "hierarchical"          => true,
                "show_ui"               => true,
                "show_in_menu"          => true,
                "show_in_nav_menus"     => true,
                "query_var"             => true,
                "rewrite"               => [
                    'slug'       => 'position_tax',
                    'with_front' => true,
                ],
                "show_admin_column"     => false,
                "show_in_rest"          => true,
                "rest_base"             => "position_tax",
                "rest_controller_class" => "WP_REST_Terms_Controller",
                "show_in_quick_edit"    => false,
            ];

            register_taxonomy("position_tax", ["career_pt"], $args);
    }
}