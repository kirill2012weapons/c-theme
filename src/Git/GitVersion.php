<?php

namespace App\Git;

/**
 * Get version of commit
 *
 * Class GitVersion
 * @package App\Git
 */
class GitVersion
{
    /**
     * @return string
     */
    public static function version() {

        exec('git describe --always',$version_mini_hash);

        return empty($version_mini_hash) ? '1.0.0' : trim($version_mini_hash[0]);
    }
}