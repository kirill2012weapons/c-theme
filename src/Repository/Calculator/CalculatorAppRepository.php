<?php

namespace App\Repository\Calculator;

use App\Model\Calculator\App\Main\StepCalculator;
use App\Model\Calculator\App\Main\ChoiceRepeater;
use App\Model\Calculator\App\Main\StepContainer;
use App\Model\Calculator\App\Main\Choice;
use App\Model\Calculator\App\Main\Step;
use App\Option\CalculatorOptions;
use App\Services\Generator\RandomStringGenerator;

class CalculatorAppRepository
{
    public function getStepCalculatorMain(): StepCalculator
    {
        return (new StepCalculator())
            ->setTitle(
                get_field('clc_app_g_icon', CalculatorOptions::MENU_SLUG)
                ? get_field('clc_app_g_icon', CalculatorOptions::MENU_SLUG)['url']
                : null
            )
            ->setImage(get_field('clc_app_g_title', CalculatorOptions::MENU_SLUG))
        ;
    }

    public function getStep1(): Step
    {
        $choices = get_field('clc_app_s1_r', CalculatorOptions::MENU_SLUG);
        $step1   = new Step();

        foreach ($choices as $choiceRow) {
            $choiceRepeater = (new ChoiceRepeater())
                ->setTitle($choiceRow['title'])
                ->setShortTitle($choiceRow['short_title'])
                ->setMultiple($choiceRow['multiple'])
            ;

            foreach ($choiceRow['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours(0)
                    ->setPercents(false)
                    ->setPricePerHour(0)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater->addChoice($choice);
            }

            $step1->addData($choiceRepeater);
        }

        return $step1;
    }

    public function getStep2(): Step
    {
        $pricePerHour    = (int)get_field('clc_app_s2_ppr', CalculatorOptions::MENU_SLUG);
        $choicesGroup1   = get_field('clc_app_s2_fr_g', CalculatorOptions::MENU_SLUG);
        $choicesGroup2   = get_field('clc_app_s2_sr_g', CalculatorOptions::MENU_SLUG);
        $choiceRepeater1 = null;
        $choiceRepeater2 = null;

        if (!empty($choicesGroup1)) {
            $choiceRepeater1 = (new ChoiceRepeater())
                 ->setTitle($choicesGroup1['title'])
                ->setShortTitle($choicesGroup1['short_title'])
                ->setMultiple($choicesGroup1['multiple'])
            ;

            foreach ($choicesGroup1['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours((int)$choiceCurrent['hours'])
                    ->setPercents(false)
                    ->setPricePerHour($pricePerHour)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater1->addChoice($choice);
            }
        }

        if (!empty($choicesGroup2)) {
            $choiceRepeater2 = (new ChoiceRepeater())
                ->setTitle($choicesGroup2['title'])
                ->setShortTitle($choicesGroup2['short_title'])
                ->setMultiple($choicesGroup2['multiple'])
            ;

            foreach ($choicesGroup2['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours(0)
                    ->setPercents((int)$choiceCurrent['percents'])
                    ->setPricePerHour($pricePerHour)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater2->addChoice($choice);
            }
        }

        return (new Step())
            ->addData($choiceRepeater1)
            ->addData($choiceRepeater2)
        ;
    }

    public function getStep3(): Step
    {
        $pricePerHour  = (int)get_field('clc_app_s3_ppr', CalculatorOptions::MENU_SLUG);
        $step3         = new Step();
        $step3Repeater = get_field('clc_app_s3_r', CalculatorOptions::MENU_SLUG);

        foreach ($step3Repeater as $step3Row) {
            $choiceRepeater = (new ChoiceRepeater())
                ->setTitle($step3Row['title'])
                ->setShortTitle($step3Row['short_title'])
                ->setMultiple($step3Row['multiple'])
            ;

            foreach ($step3Row['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours((int)$choiceCurrent['hours'])
                    ->setPercents(false)
                    ->setPricePerHour($pricePerHour)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater->addChoice($choice);
            }

            $step3->addData($choiceRepeater);
        }

        return $step3;
    }

    public function getStep4(): Step
    {
        $pricePerHour  = (int)get_field('clc_app_s4_ppr', CalculatorOptions::MENU_SLUG);
        $step4         = new Step();
        $step4Repeater = get_field('clc_app_s4_r', CalculatorOptions::MENU_SLUG);

        foreach ($step4Repeater as $step4Row) {
            $choiceRepeater = (new ChoiceRepeater())
                ->setTitle($step4Row['title'])
                ->setShortTitle($step4Row['short_title'])
                ->setMultiple($step4Row['multiple'])
            ;

            foreach ($step4Row['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours((int)$choiceCurrent['hours'])
                    ->setPercents(false)
                    ->setPricePerHour($pricePerHour)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater->addChoice($choice);
            }

            $step4->addData($choiceRepeater);
        }

        return $step4;
    }

    public function getStep5(): Step
    {
        $pricePerHour  = (int)get_field('clc_app_s5_ppr', CalculatorOptions::MENU_SLUG);
        $step5         = new Step();
        $step5Repeater = get_field('clc_app_s5_r', CalculatorOptions::MENU_SLUG);

        foreach ($step5Repeater as $step5Row) {
            $choiceRepeater = (new ChoiceRepeater())
                ->setTitle($step5Row['title'])
                ->setShortTitle($step5Row['short_title'])
                ->setMultiple($step5Row['multiple'])
            ;

            foreach ($step5Row['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours((int)$choiceCurrent['hours'])
                    ->setPercents(false)
                    ->setPricePerHour($pricePerHour)
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater->addChoice($choice);
            }

            $step5->addData($choiceRepeater);
        }

        return $step5;
    }

    public function getStep6(): Step
    {
        $step6         = new Step();
        $step6Repeater = get_field('clc_app_s6_r', CalculatorOptions::MENU_SLUG);

        foreach ($step6Repeater as $step6Row) {
            $choiceRepeater = (new ChoiceRepeater())
                ->setTitle($step6Row['title'])
                ->setShortTitle($step6Row['short_title'])
                ->setMultiple($step6Row['multiple'])
            ;

            foreach ($step6Row['choices_r'] as $choiceCurrent) {
                $choice = (new Choice())
                    ->setTitle($choiceCurrent['title'])
                    ->setDescription($choiceCurrent['description'])
                    ->setHours((int)$choiceCurrent['hours'])
                    ->setPercents(false)
                    ->setPricePerHour((int)$choiceCurrent['price_per_hour'])
                    ->setId(
                        (new RandomStringGenerator())->generate(16)
                    )
                ;

                $choiceRepeater->addChoice($choice);
            }

            $step6->addData($choiceRepeater);
        }

        return $step6;
    }

    public function getStepContainer(): StepContainer
    {
        return (new StepContainer())
            ->setStepCalculator($this->getStepCalculatorMain())
            ->setStep1($this->getStep1())
            ->setStep2($this->getStep2())
            ->setStep3($this->getStep3())
            ->setStep4($this->getStep4())
            ->setStep5($this->getStep5())
            ->setStep6($this->getStep6())
        ;
    }
}