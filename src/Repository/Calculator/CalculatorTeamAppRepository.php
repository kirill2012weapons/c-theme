<?php

namespace App\Repository\Calculator;

use App\Model\Calculator\Team\Main\CalculatorContainer;
use App\Services\Generator\RandomStringGenerator;
use App\Model\Calculator\App\Main\StepCalculator;
use App\Model\Calculator\Team\Main\Experience;
use App\Model\Calculator\Team\Main\Specialist;
use App\Option\CalculatorTeamOptions;

class CalculatorTeamAppRepository
{
    public function getStepCalculator(): StepCalculator
    {
        return (new StepCalculator())
            ->setTitle(
                get_field('cto_info_g_icon', CalculatorTeamOptions::MENU_SLUG)
                    ? get_field('cto_info_g_icon', CalculatorTeamOptions::MENU_SLUG)['url']
                    : null
            )
            ->setImage(get_field('cto_info_g_title', CalculatorTeamOptions::MENU_SLUG))
        ;
    }

    public function getArrayOfSpecialists(): array
    {
        $resultArray         = [];
        $specialistTeamField = get_field('cto_developers_r', CalculatorTeamOptions::MENU_SLUG);

        foreach ($specialistTeamField as $specialist) {
            $resultArray[] = (new Specialist())
                ->setName($specialist['specialist_name'])
                ->setPrice((int)$specialist['price_per_hour'])
                ->setId(
                    (new RandomStringGenerator())->generate(16)
                )
            ;
        }

        return $resultArray;
    }

    public function getArrayOfExperience(): array
    {
        $resultArray         = [];
        $experienceTeamField = get_field('cto_experience_r', CalculatorTeamOptions::MENU_SLUG);

        foreach ($experienceTeamField as $experience) {
            $resultArray[] = (new Experience())
                ->setName($experience['experience_name'])
                ->setInnerPercent($experience['percent'])
                ->setId(
                    (new RandomStringGenerator())->generate(16)
                )
            ;
        }

        return $resultArray;
    }

    public function getCalculatorContainer(): CalculatorContainer
    {
        return (new CalculatorContainer)
            ->setExperiences(
                $this->getArrayOfExperience()
            )
            ->setSpecialists(
                $this->getArrayOfSpecialists()
            )
            ->setStepCalculator(
                $this->getStepCalculator()
            )
        ;
    }
}