<?php

namespace App\PostType;

class MailsPostType
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        $labels = [
            "name"          => __( "Mail", "custom-post-type-ui" ),
            "singular_name" => __( "Mails", "custom-post-type-ui" ),
        ];

        $args = [
            "label"                 => __( "Mail", "custom-post-type-ui" ),
            "labels"                => $labels,
            "description"           => "",
            "public"                => false,
            "publicly_queryable"    => true,
            "show_ui"               => true,
            "show_in_rest"          => true,
            "rest_base"             => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive"           => false,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "delete_with_user"      => false,
            "exclude_from_search"   => true,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => [
                "slug"       => "mails_pt",
                "with_front" => true
            ],
            "query_var"             => true,
            "menu_icon"             => "dashicons-email",
            "supports"              => [
                "title"
            ],
        ];

        register_post_type( "mails_pt", $args );

    }
}