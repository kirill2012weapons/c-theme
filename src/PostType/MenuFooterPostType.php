<?php

namespace App\PostType;

class MenuFooterPostType
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        $labels = [
            "name"          => __( "Menus Footer", "custom-post-type-ui" ),
            "singular_name" => __( "Menu Footer", "custom-post-type-ui" ),
        ];

        $args = [
            "label"                 => __( "Menus Footer", "custom-post-type-ui" ),
            "labels"                => $labels,
            "description"           => "",
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               => true,
            "show_in_rest"          => true,
            "rest_base"             => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive"           => true,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "delete_with_user"      => false,
            "exclude_from_search"   => true,
            "capability_type"       => "post",
            "menu_position"         => 80,
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => [
                "slug"       => "menus",
                "with_front" => true
            ],
            "query_var"             => true,
            "menu_icon"             => "dashicons-welcome-widgets-menus",
            "supports"              => [
                "title"
            ],
        ];

        register_post_type( "menu_footer_pt", $args );
    }
}