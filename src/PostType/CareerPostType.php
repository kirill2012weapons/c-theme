<?php

namespace App\PostType;

class CareerPostType
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        $labels = [
            "name"          => __( "Careers", "custom-post-type-ui" ),
            "singular_name" => __( "Career", "custom-post-type-ui" ),
        ];

        $args = [
            "label"                 => __( "Careers", "custom-post-type-ui" ),
            "labels"                => $labels,
            "description"           => "",
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               => true,
            "show_in_rest"          => true,
            "rest_base"             => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive"           => true,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "delete_with_user"      => false,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => [
                "slug"       => "careers",
                "with_front" => true
            ],
            "query_var"             => true,
            "menu_icon"             => "dashicons-clipboard",
            "supports"              => [
                "title"
            ],
        ];

        register_post_type( "career_pt", $args );
    }
}