<?php

namespace App\Manager;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Option\ContactOptions;
use App\Services\FileUploader;
use App\Model\ContactModel;

class MailManager
{
    public function saveMail(ContactModel $contactModel, $postType = 'mails_pt', $customTitle = null)
    {
        $args = [
            'post_title'  => null == $customTitle
                ? $contactModel->getName() . ' - ' . $contactModel->getEmail()
                : $customTitle,
            'post_status' => 'publish',
            'post_type'   => $postType,
        ];

        $postId = wp_insert_post($args);

        if ($contactModel->getFile() && ($contactModel->getFile() instanceof UploadedFile)) {
            $attachmentId = FileUploader::upload($contactModel->getFile(), 'contact');

            update_field('file', $attachmentId,               $postId);
        } else if ($contactModel->getFile() && ($contactModel->getFile() instanceof File)) {
            $attachmentId = FileUploader::uploadFile($contactModel->getFile(), 'contact');

            update_field('file', $attachmentId,               $postId);
        }

        update_field('name',     $contactModel->getName(),    $postId);
        update_field('email',    $contactModel->getEmail(),   $postId);
        update_field('message',  $contactModel->getMessage(), $postId);

        $headers        = [
            'content-type: text/html',
        ];
        $swpsmtpOptions = get_option( 'swpsmtp_options' );
        if ($postType == 'mails_calculator_pt') {
            $attachment = '';

            if (get_field('file', $postId)) {
                $attachment = WP_CONTENT_DIR . '/uploads/contact/' . get_field('file', $postId)['filename'];
            }

            if (get_field('co_cc_g_subject', ContactOptions::MENU_SLUG)) {
                $headers[] =
                    'From: ' .
                    get_field('co_cc_g_subject', ContactOptions::MENU_SLUG) .
                    ' <' . $swpsmtpOptions['from_email_field'] . '>'
                ;
            }

            while (have_rows('co_cc_g_send_to_r', ContactOptions::MENU_SLUG)) {
                the_row();

                ob_start();
                get_template_part('template-parts/email', 'base', [
                    'name'    => $contactModel->getName(),
                    'email'   => $contactModel->getEmail(),
                    'message' => $contactModel->getMessage(),
                ]);
                $output =  ob_get_clean();

                wp_mail(
                    get_sub_field('email'),
                    get_field('co_cc_g_subject', ContactOptions::MENU_SLUG),
                    $output,
                    $headers,
                    $attachment
                );
            }
        } else if ($postType == 'mails_pt') {
            $attachment = '';

            if (get_field('file', $postId)) {
                $attachment = WP_CONTENT_DIR . '/uploads/contact/' . get_field('file', $postId)['filename'];
            }

            if (get_field('co_cm_g_subject', ContactOptions::MENU_SLUG)) {
                $headers[] =
                    'From: ' .
                    get_field('co_cm_g_subject', ContactOptions::MENU_SLUG) .
                    ' <' . $swpsmtpOptions['from_email_field'] . '>'
                ;
            }

            while (have_rows('co_cm_g_send_to_r', ContactOptions::MENU_SLUG)) {
                the_row();

                ob_start();
                get_template_part('template-parts/email', 'base', [
                    'name'    => $contactModel->getName(),
                    'email'   => $contactModel->getEmail(),
                    'message' => $contactModel->getMessage(),
                ]);
                $output =  ob_get_clean();

                wp_mail(
                    get_sub_field('email'),
                    get_field('co_cm_g_subject', ContactOptions::MENU_SLUG),
                    $output,
                    $headers,
                    $attachment
                );
            }
        }

        return true;
    }
}