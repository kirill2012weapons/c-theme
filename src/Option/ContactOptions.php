<?php

namespace App\Option;

class ContactOptions
{
    const MENU_SLUG = 'contact_settings';

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page([
                'page_title' => 'Contact Options',
                'menu_title' => 'Contact Options',
                'menu_slug'  => self::MENU_SLUG,
                'capability' => 'edit_posts',
                'redirect'	 => false,
                'icon_url'   => 'dashicons-email-alt2',
                'post_id'    => self::MENU_SLUG
            ]);
        }
    }
}