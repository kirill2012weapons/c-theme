<?php

namespace App\Option;

class MenuOptions
{
    const MENU_SLUG        = 'menu_settings';
    const MENU_SLUG_FOOTER = 'menu_settings_footer';

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page([
                'page_title' => 'Menu General',
                'menu_title' => 'Menu General',
                'menu_slug'  => self::MENU_SLUG,
                'post_id'    => self::MENU_SLUG,
                'capability' => 'edit_posts',
                'redirect'	 => '/wp-admin/admin.php?page=acf-options-burger-menu',
                'icon_url'   => 'dashicons-menu'
            ]);

            acf_add_options_sub_page([
                'page_title' 	=> 'Burger Menu',
                'menu_title'	=> 'Burger Menu',
                'parent_slug'	=> self::MENU_SLUG,
                'post_id'       => self::MENU_SLUG,
            ]);

            acf_add_options_sub_page([
                'page_title' 	=> 'Footer Menu',
                'menu_title'	=> 'Footer Menu',
                'parent_slug'	=> self::MENU_SLUG,
                'post_id'       => self::MENU_SLUG,
            ]);
        }
    }
}