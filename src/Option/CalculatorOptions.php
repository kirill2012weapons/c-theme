<?php

namespace App\Option;

class CalculatorOptions
{
    const MENU_SLUG = 'calc_settings';

    public static function init()
    {
        return new self();
    }

    public function register()
    {
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page([
                'page_title' => 'Calculator',
                'menu_title' => 'Calculator',
                'menu_slug'  => self::MENU_SLUG,
                'capability' => 'edit_posts',
                'redirect'	 => false,
                'icon_url'   => 'dashicons-calculator',
                'post_id'    => self::MENU_SLUG
            ]);
        }
    }
}