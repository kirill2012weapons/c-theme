<?php
/**
 * Template Name: About Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <?php if (get_field('ato_gg_ms_g_active')): ?>
            <div class="main-banner main-banner_about"
                <?php if (get_field('ato_gg_ms_g_background_image')): ?>
                    style="background-image: url(<?php echo get_field('ato_gg_ms_g_background_image')['url'] ?>);"
                <?php endif; ?>
            >
                <div class="container container_content h-100">
                    <div class="d-flex flex-column justify-content-center h-100 w-75 m-0-auto text-center">
                        <?php if (get_field('ato_gg_ms_g_title')): ?>
                            <div class="title-def">
                                <?php echo get_field('ato_gg_ms_g_title'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="pt-140 pb-110">
            <div class="container container_content">
                <?php if (get_field('ato_gg_chs_g_active')): ?>
                    <div>
                        <div class="w75-about m-0-auto">
                            <?php if (get_field('ato_gg_chs_g_title')): ?>
                                <div class="title-def">
                                    <?php echo get_field('ato_gg_chs_g_title'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if (get_field('ato_gg_chs_g_description')): ?>
                                <p>
                                    <?php echo get_field('ato_gg_chs_g_description'); ?>
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (have_rows('ato_gg_cs_r')): ?>
                <?php
                 $rowCount = count(get_field('ato_gg_cs_r'));
                 ?>
                    <div class="mt-150 d-none d-md-block">
                        <div class="timeline-vert">
                            <?php while (have_rows('ato_gg_cs_r')): the_row(); ?>
                                <div class="timeline-vert__item">
                                    <?php if (get_sub_field('title')): ?>
                                        <div class="timeline-vert__year <?php if (get_row_index() == $rowCount): ?> timeline-vert__year_red<?php endif; ?>">
                                            <?php echo get_sub_field('title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="timeline-vert__cont">
                                        <?php if (get_sub_field('image')): ?>
                                            <div class="timeline-vert__img">
                                                <img class="mw-100" src="<?php echo get_sub_field('image')['sizes']['thumbnail_111_111']; ?>" alt="img"/>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (get_sub_field('description')): ?>
                                        <div class="timeline-vert__text">
                                            <?php echo get_sub_field('description'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('ato_gg_ss_g_active')): ?>
                    <div class="mt-150">
                        <div class="about-grey-box">
                                <?php if (get_field('ato_gg_ss_g_title')): ?>
                                    <div class="title-def text-center mb-5">
                                        <?php echo get_field('ato_gg_ss_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('ato_gg_ss_g_description')): ?>
                                    <div class="txt-def m-0-auto text-center">
                                        <?php echo get_field('ato_gg_ss_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('ato_gg_sss_g_active')): ?>
                    <div class="mt-150">
                        <?php if (get_field('ato_gg_sss_g_title')): ?>
                            <div class="md-subtitle mb-6">
                                <?php echo get_field('ato_gg_sss_g_title'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (have_rows('ato_gg_sss_g_content_r')): ?>
                            <div class="row">
                                <?php while (have_rows('ato_gg_sss_g_content_r')): the_row(); ?>
                                    <div class="col-md-4 mb-5 d-flex d-md-block flex-column text-center text-md-left">
                                        <?php if (get_sub_field('image')): ?>
                                            <img class="icon-size" src="<?php echo get_sub_field('image')['sizes']['thumbnail_80_80']; ?>" alt="img"/>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('title')): ?>
                                            <div class="sm-subtitle mt-4 mb-4">
                                                <?php echo get_sub_field('title'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('description')): ?>
                                            <div class="txt-def">
                                                <?php echo get_sub_field('description'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('ato_gg_cis_g_active')): ?>
                    <div class="mt-150">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <?php if (get_field('ato_gg_cis_g_title')): ?>
                                    <div class="title-def">
                                        <?php echo get_field('ato_gg_cis_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_field('ato_gg_cis_g_description')): ?>
                                    <div class="txt-def mt-5">
                                        <?php echo get_field('ato_gg_cis_g_description'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if (get_field('ato_gg_cis_g_image')): ?>
                                <div class="col-md-6">
                                    <img class="mw-100" src="<?php echo get_field('ato_gg_cis_g_image')['sizes']['thumbnail_460_420']; ?>" alt="img"/>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (have_rows('ato_gg_is_r')): ?>
                    <div class="mt-150">
                        <div class="row">
                            <?php while (have_rows('ato_gg_is_r')): the_row(); ?>
                                <div class="col-md-4 mb-5 mb-md-0">
                                    <?php if (get_sub_field('image')): ?>
                                        <img class="mw-100 about-img" src="<?php echo get_sub_field('image')['sizes']['thumbnail_325_220']; ?>" alt="img"/>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('title')): ?>
                                        <div class="md-subtitle mt-5 mb-4">
                                            <?php echo get_sub_field('title'); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('description')): ?>
                                        <div class="txt-def">
                                            <?php echo get_sub_field('description'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (get_field('ato_gg_vs_g_active')): ?>
                    <div class="mt-150">
                        <div class="row align-items-center">
                            <?php if (get_field('ato_gg_vs_g_image')): ?>
                                <div class="col-md-5 d-none d-lg-block">
                                    <img class="mw-100 about-fit" src="<?php echo get_field('ato_gg_vs_g_image')['url']; ?>" alt="img"/>
                                </div>
                            <?php endif; ?>
                            <div class="col-lg-6 offset-0 offset-lg-1 d-flex flex-column">
                                <?php if (get_field('ato_gg_vs_g_title')): ?>
                                    <div class="title-def">
                                        <?php echo get_field('ato_gg_vs_g_title'); ?>
                                    </div>
                                <?php endif; ?>
                                <div class="mt-5">
                                    <?php
                                    $careerQuery = [
                                        'post_type'  => 'career_pt',
                                        'post_status'=>'publish'
                                    ];
                                    $careers      = new WP_Query($careerQuery);
                                    $careersPosts = $careers->get_posts();
                                    ?>
                                    <?php foreach ($careersPosts as $postCareer): ?>
                                        <?php /** @var $postCareer WP_Post */ ?>
                                        <?php $taxes = wp_get_post_terms($postCareer->ID, 'office_tax'); ?>
                                        <a class="vacancy-item" href="<?php echo get_post_permalink($postCareer->ID); ?>">
                                            <p class="font-weight-bold">
                                                <?php echo get_the_title($postCareer->ID); ?>
                                            </p>
                                            <?php foreach ($taxes as $tax) : ?>
                                                <?php /** @var $tax WP_Term */ ?>
                                                <div class="d-flex align-items-center txt-dark-grey">
                                                    <svg class="mr-2" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8 2C10.7536 2 13 4.37767 13 7.32845C13 9.04361 11.5134 11.2084 8.31488 13.7526L8 14C4.58408 11.3485 3 9.09894 3 7.32845C3 4.37767 5.24638 2 8 2ZM8 5C6.89543 5 6 5.89543 6 7C6 8.10457 6.89543 9 8 9C9.10457 9 10 8.10457 10 7C10 5.89543 9.10457 5 8 5Z" fill="#5E5F63"></path>
                                                    </svg>
                                                    <span><?php echo $tax->name ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                                <div class="d-flex align-items-center mt-5">
                                    <?php if (get_field('ato_gg_vs_g_left_link')) : $btn = get_field('ato_gg_vs_g_left_link'); ?>
                                        <a class="btn-def mr-5"
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                           href="<?php echo $btn['url']; ?>"
                                        >
                                            <?php echo $btn['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php if (get_field('ato_gg_vs_g_right_link')) : $btn = get_field('ato_gg_vs_g_right_link'); ?>
                                        <a class="btn-red"
                                            <?php if($btn['target'] == '_blank') echo 'target="_blank"' ?>
                                           href="<?php echo $btn['url']; ?>"
                                        >
                                            <?php echo $btn['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>
