<?php
/**
 * Template Name: AGB Template
 *
 * @package Confitech
 * @subpackage Confitech
 * @since Confitech 1.0
 */
?>

<?php get_header(); ?>

    <main>
        <div class="d-flex flex-column justify-content-center align-items-center full-h bg-light">
            <h1 class="txt-dark-grey">500</h1>
            <p class="txt-dark-grey">Server Error</p>
            <br>
            <a class="btn-def" href="<?php echo home_url(); ?>">zur Startseite</a>
            <br>
        </div>
    </main>

<?php get_footer(); ?>